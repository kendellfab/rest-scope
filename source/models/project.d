/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module models.project;

class Project {
public:
    long id;
    string name;

    this(long id, string name) {
        this.id = id;
        this.name = name;
    }
}