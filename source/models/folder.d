/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module models.folder;

import models.storagerequest;

class Folder {
public:
    long id;
    string name;
    long projectId;

    StorageRequest[] requests;

    this(long id, string name, long projectId) {
        this.id = id;
        this.name = name;
        this.projectId = projectId;
    }
}