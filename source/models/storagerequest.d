/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module models.storagerequest;

import data.guirequest;

class StorageRequest {
public:
    long id;
    string name;
    long folderId;
    GuiRequest guiRequest;

    this(long id, string name, long folderId, GuiRequest guiRequest) {
        this.id = id;
        this.name = name;
        this.folderId = folderId;
        this.guiRequest = guiRequest;
    }
}