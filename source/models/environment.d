/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module models.environment;

import data.kvpair;

class Environment {
public:
    long id;
    string name;
    KVPair[] pairs;
    long parentId;

    Environment parent;
    Environment[] children;

    this(long id, string name, KVPair[] pairs) {
        this.id = id;
        this.name = name;
        this.pairs = pairs;
    }

    this(long id, string name, KVPair[] pairs, long parentId) {
        this(id, name, pairs);
        this.parentId = parentId;
    }

    string[string] getActiveEnv() {
        string[string] activeEnv;
        if(parent !is null) {
            activeEnv = parent.getActiveEnv();
        }
        foreach(KVPair p; pairs) {
            if(p.active) {
                activeEnv[p.key] = p.value;
            }
        }
        return activeEnv;
    }
    
}