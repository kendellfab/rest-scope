/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.projectbox;

import gtk.Box;
import gtk.Label;
import gtk.ComboBox;
import gtk.Button;
import gtk.Popover;
import widgets.namenewbox;
import models.project;
import gtk.HSeparator;
import widgets.projectscombobox;
import data.projectsliststore;
import std.stdio;
import data.foldersliststore;
import widgets.folderstreeeview;
import gtk.TreePath;
import gtk.TreeViewColumn;
import gtk.TreeView;
import gtk.ScrolledWindow;
import models.folder;
import std.algorithm;
import models.storagerequest;
import data.guirequest;
import widgets.folderactionbox;
import gtk.Widget;
import widgets.requestactionbox;
import widgets.projectactionbox;

alias DoAddProject = long delegate(Project project);
alias DoAddFolder = long delegate(Folder folder);
alias ProjectSelected = void delegate(Project project);
alias DoAddRequest = long delegate(StorageRequest request, long projectId);
alias RequestSelected = void delegate(StorageRequest request);
alias RequestFolderDelete = void delegate(Folder folder, long projectId);
alias RequestDelete = void delegate(StorageRequest request, long projectId);
alias UpdateFolderName = bool delegate(Folder folder);
alias UpdateProjectName = bool delegate(Project project);
alias UpdateRequestName = bool delegate(StorageRequest request);

class ProjectBox: Box {
private:
    Popover projectAddPopover;
    DoAddProject doAddProject;
    DoAddFolder doAddFolder;
    DoAddRequest doAddRequest;
    ProjectSelected projectSelected;
    RequestSelected requestSelected;
    ProjectsListStore projectsListStore;
    ProjectsComboBox projectsComboBox;
    Project[] availableProjects;
    Project selectedProject;
    FoldersListStore foldersListStore;
    FoldersTreeView foldersTreeView;
    Popover folderAddPopover;
    Folder[] folders;
    Popover folderActionPopover;
    Folder selectedFolder;
    Button addProjectButton;
    Button addFolderButton;
    RequestFolderDelete requestFolderDelete;
    Popover requestActionPopover;
    RequestDelete requestDelete;
    UpdateFolderName updateFolderName;
    Popover editProjectPopover;
    Button editProjectButton;
    UpdateProjectName updateProjectName;
    UpdateRequestName updateRequestName;


public:
    this() {
        super(GtkOrientation.VERTICAL, 2);
        setMarginRight(10);

        auto projectListBox = new Box(GtkOrientation.HORIZONTAL, 2);
        projectsListStore = new ProjectsListStore();
        projectsComboBox = new ProjectsComboBox(projectsListStore);
        projectsComboBox.setHexpand(true);
        projectsComboBox.setSizeRequest(200, 0);
        projectsComboBox.addOnChanged(&projectChanged);
        addProjectButton = new Button("list-add", GtkIconSize.MENU);
        editProjectButton = new Button("document-edit", GtkIconSize.MENU);
        editProjectButton.setSensitive(false);
        projectListBox.add(projectsComboBox);
        projectListBox.add(addProjectButton);
        projectListBox.add(editProjectButton);
        add(projectListBox);
        addProjectButton.setSensitive(false);

        projectAddPopover = new Popover(addProjectButton);
        projectAddPopover.add(new NameNewBox(&doAddProjectName, "Project Name"));
        addProjectButton.addOnClicked(delegate (Button _) {
            projectAddPopover.showAll();
        });

        editProjectPopover = new Popover(editProjectButton);
        editProjectButton.addOnClicked(delegate (Button _) {
            editProjectPopover.removeAll();
            editProjectPopover.add(new ProjectActionBox(selectedProject.name, delegate (string projectName) {
                selectedProject.name = projectName;
                if(updateProjectName(selectedProject)) {
                    editProjectPopover.hide();
                    projectsComboBox.updateProjectName(projectName);
                }
            }));
            editProjectPopover.showAll();
        });

        auto projSep = new HSeparator();
        projSep.setMarginTop(10);
        projSep.setMarginBottom(10);
        add(projSep);

        auto auxBox = new Box(GtkOrientation.HORIZONTAL, 2);
        addFolderButton = new Button("folder-new", GtkIconSize.MENU);
        addFolderButton.setTooltipText("Click the add button to add a new folder.  Select the new folder in the list to add a request.");
        addFolderButton.setSensitive(false);

        folderAddPopover = new Popover(addFolderButton);
        folderAddPopover.add(new NameNewBox(&doAddFolderName, "Folder Name"));
        addFolderButton.addOnClicked(delegate (Button _) {
            folderAddPopover.showAll();
        });

        auxBox.packStart(addFolderButton, false, false, 0);

        add(auxBox);

        foldersListStore = new FoldersListStore();
        foldersTreeView = new FoldersTreeView(foldersListStore);
        foldersTreeView.setShowExpanders(true);
        foldersTreeView.setActivateOnSingleClick(true);
        foldersTreeView.addOnButtonPress(&rowRightClick);

        foldersTreeView.addOnRowActivated(&rowActivated);
        auto foldersScrolledWindow = new ScrolledWindow();
        foldersScrolledWindow.setVexpand(true);
        foldersScrolledWindow.add(foldersTreeView);
        add(foldersScrolledWindow);
    }

    bool rowRightClick(GdkEventButton* event, Widget _) {
        if(event.button == 3) {
            writeln("On click");
            TreePath path;
            TreeViewColumn column;
            int cellX;
            int cellY;
            foldersTreeView.getPathAtPos(cast(int)event.x, cast(int)event.y, path, column, cellX, cellY);

            if(path.getDepth() == 2) {
                auto rcRequest = folders[path.getIndices[0]].requests[path.getIndices[1]];

                GdkRectangle rect;
                foldersTreeView.getCellArea(path, column, rect);
                rect.y += 35;
                requestActionPopover = new Popover(foldersTreeView);
                requestActionPopover.setPointingTo(&rect);
                requestActionPopover.add(new RequestActionBox(rcRequest, delegate(StorageRequest req) {
                    requestActionPopover.hide();
                    requestDelete(req, selectedProject.id);
                }, delegate(string requestName) {
                    requestActionPopover.hide();
                    rcRequest.name = requestName;
                    updateRequestName(rcRequest);
                    foldersListStore.updateRequestName(requestName, path.toString());
                }));
                requestActionPopover.showAll();

            }
            return true;
        }

        return false;
    }

    void rowActivated(TreePath path, TreeViewColumn column, TreeView tree) {
        if(path.getDepth() == 1) {
            string rowPath = path.toString();
            selectedFolder = folders[path.getIndices[0]];

            GdkRectangle rect;
            tree.getCellArea(path, column, rect);
            rect.y += 20;
            folderActionPopover = new Popover(tree);
            folderActionPopover.setPointingTo(&rect);
            folderActionPopover.add(new FolderActionBox(delegate(string name) {
                folderActionPopover.hide();
                StorageRequest request = new StorageRequest(0, name, selectedFolder.id, new GuiRequest());
                long id = doAddRequest(request, selectedProject.id);
                request.id = id;
                foldersListStore.addRequestToFolder(rowPath, request);
                selectedFolder.requests ~= request;
            }, &handleFolderDeleteRequest, delegate(string folderName) {
                selectedFolder.name = folderName;
                if(updateFolderName(selectedFolder)) {
                    folderActionPopover.hide();
                    foldersListStore.updateFolderName(folderName, rowPath);
                }
            },  "New Request Name", "New Folder Name", selectedFolder.name));
            folderActionPopover.showAll();
        } else {
            auto selectedRequest = folders[path.getIndices[0]].requests[path.getIndices[1]];
            requestSelected(selectedRequest);
        }
    }

    void projectChanged(ComboBox _) {
        const auto active = projectsComboBox.getActive();
        if(active >= 0) {
            selectedProject = availableProjects[active];
            projectSelected(selectedProject);
        }
    }

    void doAddProjectName(string name) {
        auto project = new Project(0, name);
        long id = doAddProject(project);
        projectAddPopover.hide();
        project.id = id;
        projectsListStore.addProject(project);
        availableProjects ~= project;
        projectsComboBox.setActive(cast(int)availableProjects.length - 1);
        enableButtons();
    }

    void doAddFolderName(string name) {
        auto folder = new Folder(0, name, selectedProject.id);
        long id = doAddFolder(folder);
        folderAddPopover.hide();
        folder.id = id;
        foldersListStore.addFolder(folder);
        folders ~= folder;
    }

    void handleFolderDeleteRequest() {
        requestFolderDelete(selectedFolder, selectedProject.id);
        folderActionPopover.hide();
    }

    void setDoAddProject(DoAddProject doAddProject) {
        this.doAddProject = doAddProject;
    }

    void setDoAddFolder(DoAddFolder doAddFolder) {
        this.doAddFolder = doAddFolder;
    }

    void setProjectSelected(ProjectSelected projectSelected) {
        this.projectSelected = projectSelected;
    }

    void setDoAddRequest(DoAddRequest doAddRequest) {
        this.doAddRequest = doAddRequest;
    }

    void setRequestSelected(RequestSelected rs) {
        this.requestSelected = rs;
    }

    void setRequestFolderDelete(RequestFolderDelete req) {
        this.requestFolderDelete = req;
    }

    void setRequestDelete(RequestDelete reqDel) {
        this.requestDelete = reqDel;
    }

    void setUpdateFolderName(UpdateFolderName updateFolderName) {
        this.updateFolderName = updateFolderName;
    }

    void setUpdateProjectName(UpdateProjectName updateProjectName) {
        this.updateProjectName = updateProjectName;
    }

    void setUpdateRequestName(UpdateRequestName updateRequestName) {
        this.updateRequestName = updateRequestName;
    }

    void setProjects(Project[] projects) {
        projectsListStore.clear();
        foldersListStore.clear();
        availableProjects = projects;

        if(projects.length > 0) {
            foreach(Project project; projects) {
                projectsListStore.addProject(project);
            }
            projectsComboBox.setActive(0);
        }
    }

    void setFolders(Folder[] folders) {
        foldersListStore.clear();
        this.folders = folders;
        foreach(Folder folder; folders) {
            foldersListStore.addFolder(folder);
        }
    }

    void enableButtons() {
        addProjectButton.setSensitive(true);
        
        if(availableProjects !is null && availableProjects.length > 0) {
            editProjectButton.setSensitive(true);
            addFolderButton.setSensitive(true);
        } else {
            addFolderButton.setSensitive(false);
        }
    }
}