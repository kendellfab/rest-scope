/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.bodybox;

import gtk.Box;
import widgets.bodytypecomobobox;
import gtk.ComboBox;
import std.stdio;
import widgets.keyvaluelist;
import gsv.SourceView;
import gsv.SourceLanguageManager;
import gsv.SourceLanguage;
import gsv.SourceBuffer;
import data.bodytypeliststore;
import data.bodytype;
import data.kvpair;
import gtk.Entry;
import gtk.Button;
import gtk.ApplicationWindow;
import callbacks.guirequestupdatecallback;
import gtk.Widget;
import gdk.Event;
import gsv.SourceStyleScheme;

enum BodyDisplayType {
    None, KeyValuePair, RawContent, File
}

alias GetBodyFilePathCallback = void delegate(string key);

class BodyBox: Box {

private:
    BodyTypeComboBox bodyTypeComboBox;
    KeyValueList keyValueList;
    SourceView rawBody;
    Box fileInputBox;
    Entry fileKey;
    Entry fileEntry;
    Button fileButton;
    GetBodyFilePathCallback getBodyFilePathCallback;
    GuiRequestUpdateCallback guiRequestUpdateCallback;
    SourceStyleScheme scheme;

    void initRawBody() {
        rawBody = new SourceView();
        rawBody.setShowLineNumbers(true);
        rawBody.setHexpand(true);
        rawBody.setVexpand(true);
        rawBody.setHighlightCurrentLine(true);
        rawBody.addOnFocusOut((Event e, Widget w) {
            if(guiRequestUpdateCallback !is null) {
                guiRequestUpdateCallback.requestUpdated();
            }
            return false;
        });

        if(scheme !is null) {
            rawBody.getBuffer().setStyleScheme(scheme);
        }

        add(rawBody);
    }

    void initFileBox() {
        fileInputBox = new Box(GtkOrientation.HORIZONTAL, 2);
        fileInputBox.setMarginLeft(5);
        fileInputBox.setMarginTop(5);
        fileInputBox.setMarginRight(5);
        fileInputBox.setHexpand(true);

        fileKey = new Entry();
        fileKey.setHexpand(true);
        fileKey.setPlaceholderText("Form key for file.  Default: file");
        fileInputBox.add(fileKey);

        fileEntry = new Entry();
        fileEntry.setHexpand(true);

        fileButton = new Button("Select File");
        fileButton.addOnClicked(&fileButtonClicked);

        fileInputBox.add(fileEntry);
        fileInputBox.add(fileButton);
        add(fileInputBox);
        fileInputBox.showAll();
    }

public:
    this(GuiRequestUpdateCallback guiRequestUpdateCallback) {
        super(GtkOrientation.VERTICAL, 2);
        this.guiRequestUpdateCallback = guiRequestUpdateCallback;

        bodyTypeComboBox = new BodyTypeComboBox();
        bodyTypeComboBox.addOnChanged(&bodyTypeChanged);
        bodyTypeComboBox.setMarginTop(2);
        bodyTypeComboBox.setMarginStart(2);
        bodyTypeComboBox.setMarginEnd(2);
        bodyTypeComboBox.setMarginBottom(5);

        add(bodyTypeComboBox);

        keyValueList = new KeyValueList();
        keyValueList.setGuiRequestUpdateCallback(guiRequestUpdateCallback);
        add(keyValueList);
    }

    void fileButtonClicked(Button _) {
        getBodyFilePathCallback("file");
    }

    void setFilePath(string key, string path) {
        fileEntry.setText(path);
        if(guiRequestUpdateCallback !is null) {
            guiRequestUpdateCallback.requestUpdated();
        }
    }

    void bodyTypeChanged(ComboBox _) {
        if(fileEntry is null) {
            initFileBox();
        }
        if(rawBody is null) {
            initRawBody();
        }
        switch(getBodyDisplayType()) {
            case BodyDisplayType.None:
                keyValueList.setVisible(false);
                rawBody.setVisible(false);
                fileInputBox.setVisible(false);
            break;
            case BodyDisplayType.KeyValuePair:
                keyValueList.setVisible(true);
                rawBody.setVisible(false);
                fileInputBox.setVisible(false);
            break;
            case BodyDisplayType.File:
                keyValueList.setVisible(false);
                rawBody.setVisible(false);
                fileInputBox.setVisible(true);
            break;
            default:
                keyValueList.setVisible(false);
                rawBody.setVisible(true);
                fileInputBox.setVisible(false);
        }
    }

    BodyDisplayType getBodyDisplayType() {
        const auto bodyType = bodyTypeComboBox.getBodyType();
        if(bodyType == BodyTypeListStore.items[0]) {
            return BodyDisplayType.None;
        } else if(bodyType == BodyTypeListStore.items[1] || bodyType == BodyTypeListStore.items[2]) {
            return BodyDisplayType.KeyValuePair;
        } else if(bodyType == BodyTypeListStore.items[7]) {
            return BodyDisplayType.File;
        } else {
            return BodyDisplayType.RawContent;
        }
    }

    BodyType getBodyType() {
        const auto bodyType = bodyTypeComboBox.getBodyType();
        if(bodyType == BodyTypeListStore.items[0]) {
            return BodyType.None;
        } else if(bodyType == BodyTypeListStore.items[1]) {
            return BodyType.Multipart;
        } else if (bodyType == BodyTypeListStore.items[2]) {
            return BodyType.Form;
        } else if(bodyType == BodyTypeListStore.items[3]) {
            return BodyType.JSON;
        } else if(bodyType == BodyTypeListStore.items[4]) {
            return BodyType.XML;
        } else if(bodyType == BodyTypeListStore.items[5]) {
            return BodyType.YAML;
        } else if(bodyType == BodyTypeListStore.items[6]) {
            return BodyType.OTHER;
        } else {
            return BodyType.File;
        }
    }

    KVPair[] getKVPairs() {
        return keyValueList.getPairs();
    }

    string getRawBody() {
        if(rawBody is null) {
            return "";
        }
        return rawBody.getBuffer().getText();
    }

    string getFilePath() {
        if(fileEntry is null) {
            return "";
        }
        return fileEntry.getText();
    }

    string getFileKey() {
        if(fileKey is null) {
            return "";
        }
        return fileKey.getText();
    }

    public void reset() {
        keyValueList.reset();
        if(rawBody !is null) {
            rawBody.getBuffer().setText("");
        }
        bodyTypeComboBox.setActive(0);
        if(fileEntry !is null) {
            fileEntry.setText("");
        }
        if(fileKey !is null) {
            fileKey.setText("");
        }
    }

    public void updateBody(BodyType bt, KVPair[] bodyPairs, string bodyRaw, string bodyFilePath, string bodyFileKey) {
        switch(bt) {
            case BodyType.None:
                bodyTypeComboBox.setActive(0);
            break;
            case BodyType.Multipart:
                bodyTypeComboBox.setActive(1);
                keyValueList.updatePairs(bodyPairs);
            break;
            case BodyType.Form:
                bodyTypeComboBox.setActive(2);
                keyValueList.updatePairs(bodyPairs);
            break;
            case BodyType.JSON:
                bodyTypeComboBox.setActive(3);
                rawBody.getBuffer.setText = bodyRaw;
            break;
            case BodyType.XML:
                bodyTypeComboBox.setActive(4);
                rawBody.getBuffer.setText = bodyRaw;
            break;
            case BodyType.YAML:
                bodyTypeComboBox.setActive(5);
                rawBody.getBuffer.setText = bodyRaw;
            break;
            case BodyType.OTHER:
                bodyTypeComboBox.setActive(6);
                rawBody.getBuffer.setText = bodyRaw;
            break;
            case BodyType.File:
                bodyTypeComboBox.setActive(7);
                fileEntry.setText(bodyFilePath is null ? "" : bodyFilePath);
                fileKey.setText(bodyFileKey is null ? "" : bodyFileKey);
            break;
            default:
            break;
        }

    }

    public void setGetBodyFilePathCallback(GetBodyFilePathCallback callback) {
        getBodyFilePathCallback = callback;
    }

    public void setScheme(SourceStyleScheme scheme) {
        this.scheme = scheme;
        if(rawBody !is null) {
            rawBody.getBuffer().setStyleScheme(scheme);
        }
    }
}