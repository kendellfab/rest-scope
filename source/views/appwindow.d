/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.appwindow;

import gtk.ApplicationWindow : ApplicationWindow;
import gtk.Application;
import gtk.HeaderBar;
import gtk.Box;
import gtk.Entry;
import gtk.Button;
import gtk.Paned;
import std.stdio;
import requests;
import widgets.keyvaluelist;
import data.kvpair;
import glib.Idle;
import widgets.methodcombobox;
import data.guirequest;
import gdkpixbuf.Pixbuf : Pixbuf;
import data.icon : ICON_XMP;
import managers.requestor;
import std.algorithm;
import gtk.Notebook;
import std.string;
import data.keyvalueliststore;
import widgets.keyvaluetreeview;
import views.resultsbox;
import views.bodybox;
import gtk.MenuButton;
import gtk.Popover;
import gtk.Image;
import gio.MenuItem;
import gio.Menu;
import gio.SimpleAction;
import glib.Variant : GlibVariant = Variant;
import views.authbox;
import views.projectbox;
import gtk.FileChooserNative;
import gtk.FileFilter;
import managers.storage;
import models.project;
import models.folder;
import models.storagerequest;
import views.environmentwindow;
import gtk.ComboBox;
import widgets.environmentscombobox;
import models.environment;
import data.guiresponse;
import gtk.HSeparator;
import settings.manager;
import gsv.SourceView;
import gtk.ScrolledWindow;
import gtk.AboutDialog;
import gtk.Dialog;
import callbacks.guirequestupdatecallback;
import gdk.Event;
import gtk.Widget;
import asdf;
import callbacks.environmentmanagementcallback;
import gsv.StyleSchemeChooserWidget;
import gsv.SourceStyleSchemeManager;
import gsv.SourceStyleScheme;
import gtk.Dialog;
import gobject.Signals;
import gtk.MessageDialog;
import callbacks.historycallback;

import core.thread;
import std.concurrency;
import std.parallelism;

class MainWindow : ApplicationWindow, GuiRequestUpdateCallback, EnvironmentManagementCallback, HistoryCallback {

    private MethodComboBox methodComboBox;
    private Entry urlEntry;
    private Button goButton;
    private KeyValueList queryValueList;
    private KeyValueList headerValueList;
    private ResultsBox resultsBox;
    private BodyBox bodyBox;
    private AuthBox authBox;
    private ProjectBox projectBox;
    private StorageManager storageManager;
    private Notebook entryNotebook;
    private HeaderBar hb;
    private StorageRequest selectedStorageRequest;
    private EnvironmentWindow envWindow;
    private Button envButton;
    private EnvironmentsComboBox envComboBox;
    private SettingsManager settingsManager;
    private SourceView notes;
    private SourceStyleSchemeManager styleSchemeManager;

    private bool idleHandlerEnabled = false;
    private bool isUpdating = false;

    this(Application application, SettingsManager settingsManager) {
        super(application);
        this.settingsManager = settingsManager;
        this.styleSchemeManager = new SourceStyleSchemeManager();
        initUI();
        if(settingsManager.getLastDB() !is "") {
            openDBPath(settingsManager.getLastDB());
        }
    }

    private void initUI() {
        this.setDefaultSize(1450, 850);
        setDefaultIcon(new Pixbuf(ICON_XMP));

        setupHeaderBar();
        
        auto inputVBox = new Box(GtkOrientation.VERTICAL, 2);
        inputVBox.setMarginRight(10);
        inputVBox.setHexpand(true);
        auto entryBox = new Box(GtkOrientation.HORIZONTAL, 2);

        methodComboBox = new MethodComboBox();
        methodComboBox.setHexpand(false);
        methodComboBox.addOnChanged((ComboBox _) {
            requestUpdated();
        });

        urlEntry = new Entry();
        urlEntry.setPlaceholderText("Enter URL...");
        urlEntry.setMarginLeft(5);
        urlEntry.setMarginRight(5);
        urlEntry.setHexpand(true);
        urlEntry.addOnFocusOut((Event e, Widget w) {
            requestUpdated();
            return true;
        });

        goButton = new Button("Go");
        goButton.addOnClicked(&goButtonClicked);
        entryBox.add(methodComboBox);
        entryBox.add(urlEntry);
        entryBox.add(goButton);
        entryBox.setHexpand(true);
        inputVBox.add(entryBox);

        auto projSep = new HSeparator();
        projSep.setMarginTop(10);
        projSep.setMarginBottom(10);
        inputVBox.add(projSep);

        entryNotebook = new Notebook();
        entryNotebook.setVexpand(true);
        entryNotebook.setMarginTop(8);
        queryValueList = new KeyValueList();
        queryValueList.setGuiRequestUpdateCallback(this);
        entryNotebook.appendPage(queryValueList, "Query Params");

        headerValueList = new KeyValueList();
        headerValueList.setGuiRequestUpdateCallback(this);
        entryNotebook.appendPage(headerValueList, "Headers");
        
        bodyBox = new BodyBox(this);
        bodyBox.setGetBodyFilePathCallback(&getBodyFilePath);
        entryNotebook.appendPage(bodyBox, "Body");

        authBox = new AuthBox(this);
        entryNotebook.appendPage(authBox, "Authentication");
        
        notes = new SourceView();
        notes.setMarginTop(2);
        notes.addOnFocusOut((Event e, Widget w) {
            requestUpdated();
            return false;
        });
        notes.setShowLineNumbers(true);
        notes.setWrapMode(GtkWrapMode.WORD_CHAR);
        notes.setHighlightCurrentLine(true);
        auto notesScroll = new ScrolledWindow();
        notesScroll.add(notes);
        entryNotebook.appendPage(notesScroll, "Notes");

        inputVBox.add(entryNotebook);

        resultsBox = new ResultsBox(this);

        auto insidePaned = new Paned(GtkOrientation.HORIZONTAL);
        // insidePaned.add(inputVBox, resultsBox);
        insidePaned.pack1(inputVBox, false, false);
        insidePaned.pack2(resultsBox, true, true);
        insidePaned.setMarginLeft(10);

        projectBox = new ProjectBox();
        projectBox.setDoAddProject(&doAddProject);
        projectBox.setDoAddFolder(&doAddFolder);
        projectBox.setProjectSelected(&projectSelected);
        projectBox.setDoAddRequest(&doAddRequest);
        projectBox.setRequestSelected(&requestSelected);
        projectBox.setRequestFolderDelete(&handleFolderDeleteRequest);
        projectBox.setRequestDelete(&handleRequestDelete);
        projectBox.setUpdateFolderName(&updateFolderName);
        projectBox.setUpdateProjectName(&updateProjectName);
        projectBox.setUpdateRequestName(&updateRequestName);

        auto outsideBox = new Paned(GtkOrientation.HORIZONTAL);
        outsideBox.setMarginTop(10);
        outsideBox.setMarginBottom(10);
        outsideBox.setMarginLeft(10);
        outsideBox.setMarginRight(10);
        outsideBox.pack1(projectBox, false, false);
        outsideBox.pack2(insidePaned, true, true);
        this.add(outsideBox);

        updateSchemes();
    }

    private void updateSchemes() {
        auto scheme = getScheme();
        resultsBox.updateScheme(scheme);
        notes.getBuffer().setStyleScheme(scheme);
        bodyBox.setScheme(scheme);
    }

    private SourceStyleScheme getScheme() {
        auto allSchemes = styleSchemeManager.getSchemeIds();
        auto schemeID = settingsManager.getSchemeID();
        auto scheme = styleSchemeManager.getScheme(schemeID == "" || schemeID is null ? allSchemes[0] : schemeID);
        return scheme;
    }

    private void setupHeaderBar() {
        hb = new HeaderBar();
        hb.setShowCloseButton(true);
        hb.setTitle("Rest Scope");
        hb.setSubtitle("Displaying a subtitle.");

        auto actionReset = new SimpleAction("reset", null);
        actionReset.addOnActivate(delegate(_, SimpleAction){
            writeln("Clicked Reset");
            reset();
        });
        addAction(actionReset);

        auto actionTheme = new SimpleAction("theme", null);
        actionTheme.addOnActivate(delegate(_, SimpleAction) {
            auto schemeChooser = new StyleSchemeChooserWidget();
            Signals.connect(schemeChooser, "notify::style-scheme", (Event _) {
                settingsManager.setSchemeID(schemeChooser.getStyleScheme().getId());
                updateSchemes();
            });

            auto schemeDialog = new Dialog();
            schemeDialog.setTitle("Scheme Chooser");
            schemeDialog.getContentArea().add(schemeChooser);
            schemeDialog.showAll();
        });
        addAction(actionTheme);

        auto actionAboutDialog = new SimpleAction("about", null);
        actionAboutDialog.addOnActivate(delegate(_, SimpleAction) {
            displayAboutDialog();
        });
        addAction(actionAboutDialog);
        
        auto viewMoreButton = new MenuButton();
        auto imageViewMore = new Image("view-more", GtkIconSize.LARGE_TOOLBAR);
        viewMoreButton.add(imageViewMore);
        viewMoreButton.setValign(GtkAlign.CENTER);
        hb.packEnd(viewMoreButton);

        auto menuModel = new Menu();
        menuModel.append("Reset", "win.reset");
        menuModel.append("Scheme", "win.theme");
        menuModel.append("About", "win.about");

        Popover po = new Popover(viewMoreButton, menuModel);
        viewMoreButton.setPopover(po);

        auto filesButton = new MenuButton();
        filesButton.setValign(GtkAlign.CENTER);
        auto imageFiles = new Image("pan-down", GtkIconSize.LARGE_TOOLBAR);
        filesButton.add(imageFiles);
        hb.packStart(filesButton);

        auto actionOpen = new SimpleAction("open", null);
        actionOpen.addOnActivate(&openDB);
        addAction(actionOpen);

        auto actionNew = new SimpleAction("new", null);
        actionNew.addOnActivate(&newDB);
        addAction(actionNew);
        auto filesModel = new Menu();
        filesModel.append("Open", "win.open");
        filesModel.append("New", "win.new");

        Popover filesPopover = new Popover(filesButton, filesModel);
        filesButton.setPopover(filesPopover);

        envButton = new Button("open-menu", GtkIconSize.LARGE_TOOLBAR);
        envButton.setValign(GtkAlign.CENTER);
        hb.packEnd(envButton);
        envButton.setSensitive(false);

        envButton.addOnClicked(&envButtonClicked);

        envComboBox = new EnvironmentsComboBox(settingsManager.getSelectedEnvironmentID());
        envComboBox.addOnChanged(&environmentChanged);
        hb.packEnd(envComboBox);

        this.setTitlebar(hb);
    }

    void displayAboutDialog() {
        AboutDialog dialog;

        with (dialog = new AboutDialog()) {
            setTransientFor(this);
            setDestroyWithParent(true);
            setModal(true);

            setWrapLicense(true);
            setLogoIconName(null);
            setProgramName("Rest Scope");
            setComments("A gnome native rest tester for those who don't like electron.");
            setVersion("0.35");
            setCopyright("Copyright \xc2\xa9 2020 Kendell Fabricius");
            setLicense("This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.");

            addOnClose(delegate(Dialog dlg) {
                dlg.destroy();
            });
            present();
        }
    }

    void environmentChanged(ComboBox _) {
        auto env = envComboBox.getSelectedEnv();
        if(env !is null) {
            settingsManager.setSelectedEnvironmentID(env.id);
        }
    }

    void envButtonClicked(Button _) {
        if(envWindow !is null) {
            return;
        }
        envWindow = new EnvironmentWindow(this);
        envWindow.addOnDestroy(delegate(Widget) {
            envWindow = null;
        });
        envWindow.setEnvironments(storageManager.listHierarchy());
        envWindow.showAll();
        envWindow.present;
    }

    public Environment[] doAddEnvironment(Environment env) {
        storageManager.addEnvironment(env);
        auto envs = storageManager.listHierarchy();
        envComboBox.addEnvironments(envs);
        return envs;
    }

    public Environment[] doUpdateEnvironment(Environment env) {
        storageManager.updateEnvironment(env);
        auto envs = storageManager.listHierarchy();
        envComboBox.addEnvironments(envs);
        return envs;
    }

    public Environment[] doDeleteEnvironment(Environment env) {
        storageManager.deleteEnvironment(env);
        auto envs = storageManager.listHierarchy();
        envComboBox.addEnvironments(envs);
        return envs;
    }

    public void saveSelectedScheme(string schemeID) {
        settingsManager.setSchemeID(schemeID);
    }

    public long doAddProject(Project project) {
        long res = storageManager.addProject(project);
        return res;
    }

    public long doAddFolder(Folder folder) {
        long res = storageManager.addFolder(folder);
        return res;
    }

    public long doAddRequest(StorageRequest request, long projectId) {
        long res = storageManager.addRequest(request);
        return res;
    }

    public void projectSelected(Project project) {
        projectBox.setFolders(storageManager.listFolders(project.id));
    }

    public void handleFolderDeleteRequest(Folder folder, long projectId) {
        storageManager.deleteFolder(folder);
        projectBox.setFolders(storageManager.listFolders(projectId));
    }

    public void handleRequestDelete(StorageRequest req, long projectId) {
        storageManager.deleteRequest(req);
        projectBox.setFolders(storageManager.listFolders(projectId));
    }

    public bool updateFolderName(Folder folder) {
        storageManager.updateFolder(folder);
        return true;
    }

    public bool updateProjectName(Project project) {
        storageManager.updateProject(project);
        return true;
    }

    public bool updateRequestName(StorageRequest request) {
        storageManager.updateRequest(request);
        if(this.selectedStorageRequest !is null && this.selectedStorageRequest.id == request.id) {
            this.selectedStorageRequest.name = request.name;
            hb.setSubtitle(selectedStorageRequest.name);
        }
        return true;
    }

    GuiResponse[] listResponses() {
        if(selectedStorageRequest !is null) {
            return storageManager.listResponsesForRequest(selectedStorageRequest.id);
        }
        return null;
    }

    bool deleteResponse(GuiResponse response) {
        storageManager.deleteResponse(response);
        return true;
    }

    bool clearAllResponsesForRequest() {
        storageManager.clearAllResponsesForRequest(selectedStorageRequest.id);
        return true;
    }

    void responseSelected(GuiResponse response) {
        resultsBox.updateForResponse(response);
    }

    public void requestUpdated() {
        saveGuiRequest(null);
    }

    public void requestSelected(StorageRequest request) {
        isUpdating = true;
        // Setting this to null, as the reset changes the combo box changed callback and saves an empty guirequest.
        // A null selected storage request will fail the test in the save method and therefore do nothing.
        this.selectedStorageRequest = null;
        reset();
        this.selectedStorageRequest = request;
        this.selectedStorageRequest.guiRequest = storageManager.loadGuiRequest(this.selectedStorageRequest.id);
        showSelectedRequest();
        isUpdating = false;
    }

    private void showSelectedRequest() {
        hb.setSubtitle(selectedStorageRequest.name);
        if(selectedStorageRequest.guiRequest.url !is null) {
            urlEntry.setText(selectedStorageRequest.guiRequest.url);
        }
        methodComboBox.updateActive(selectedStorageRequest.guiRequest.method);
        queryValueList.updatePairs(selectedStorageRequest.guiRequest.query);
        headerValueList.updatePairs(selectedStorageRequest.guiRequest.headers);
        bodyBox.updateBody(selectedStorageRequest.guiRequest.bodyType, selectedStorageRequest.guiRequest.bodyPairs, 
            selectedStorageRequest.guiRequest.bodyRaw, selectedStorageRequest.guiRequest.getFilePath, selectedStorageRequest.guiRequest.getFileKey);
        authBox.updateAuth(selectedStorageRequest.guiRequest.authType, selectedStorageRequest.guiRequest.auth);
        notes.getBuffer().setText(selectedStorageRequest.guiRequest.getNotes is null ? "" : selectedStorageRequest.guiRequest.getNotes);
        entryNotebook.setCurrentPage(0);

        GuiResponse gr = storageManager.getLatestResponse(selectedStorageRequest.id);
        if(gr !is null) {
            resultsBox.updateForResponse(gr);
        }
    }

    private void openDB(GlibVariant, SimpleAction) {
        writeln("Clicked open");

        FileChooserNative dialog = new FileChooserNative("Open Database", this, GtkFileChooserAction.OPEN, "Open", "Cancel");
        dialog.setLocalOnly(true);
        auto filter = new FileFilter();
        filter.addPattern("*.restscope");
        filter.setName("Rest Scope");
        dialog.addFilter(filter);
        int response = dialog.run();
        if(response == GtkResponseType.ACCEPT) {
            reset();
            string fileName = dialog.getFilename();
            writefln("Open file: %s", fileName);
            openDBPath(fileName);
        }

        dialog.destroy();

    }

    private void openDBPath(string fileName) {
        writeln(fileName);
        if(storageManager !is null) {
            storageManager.close();
            storageManager = null;
        }
        try {
            settingsManager.setLastDB(fileName);
            storageManager = new StorageManager(fileName);
            projectBox.setProjects(storageManager.listProjects);
            projectBox.enableButtons();
            envButton.setSensitive(true);
            envComboBox.addEnvironments(storageManager.listHierarchy());
        } catch(Exception e) {
            writeln(e);
            auto msgDialog = new MessageDialog(null, GtkDialogFlags.DESTROY_WITH_PARENT, GtkMessageType.ERROR, GtkButtonsType.CLOSE, "Error opening rest scope database.");
            msgDialog.run();
            msgDialog.destroy();
            settingsManager.setLastDB("");
        }
    }

    private void newDB(GlibVariant, SimpleAction) {

        FileChooserNative dialog = new FileChooserNative("New Database", this, GtkFileChooserAction.SAVE, "Save", "Cancel");
        int response = dialog.run();
        if(response == GtkResponseType.ACCEPT) {
            string fileName = dialog.getFilename();
            writefln("New database: %s", fileName);
            fileName ~= ".restscope";
            projectBox.enableButtons();
            if(storageManager !is null) {
                reset();
                storageManager.close();
                storageManager = null;
            }
            settingsManager.setLastDB(fileName);
            storageManager = new StorageManager(fileName);
            projectBox.setProjects(storageManager.listProjects);
            envComboBox.reset();
            envButton.setSensitive(true);
        }
        dialog.destroy();
    }

    public void getBodyFilePath(string key) {
        FileChooserNative dialog = new FileChooserNative("Request File", this, GtkFileChooserAction.OPEN, "Open", "Cancel");
        int response = dialog.run();
        if(response == GtkResponseType.ACCEPT) {
            auto fileName = dialog.getFilename();
            bodyBox.setFilePath(key, fileName);
        }
    }

    public void reset() {
        selectedStorageRequest = null;
        urlEntry.setText("");
        methodComboBox.setActive(0);
        queryValueList.reset();
        headerValueList.reset();
        bodyBox.reset();
        resultsBox.reset();
        authBox.reset();
    }

    private void goButtonClicked(Button button) {
        resultsBox.setBusySpinner(true);
        idleHandlerEnabled = true;
        auto idler = new Idle(&checkPendingRequest);
        auto guiRequest = uiToRequest();
        saveGuiRequest(guiRequest);
        string[string] activeEnv;
        auto selectedEnv = envComboBox.getSelectedEnv();
        if(selectedEnv !is null) {
            if(selectedEnv.parentId !is 0) {
                selectedEnv.parent = storageManager.loadEnvironment(selectedEnv.parentId);
            }
            activeEnv = selectedEnv.getActiveEnv();
        }
        auto requester = new Requestor(guiRequest, activeEnv);
        requester.doRequest();        
    }

    private void saveGuiRequest(GuiRequest guiRequest) {
        // I only want this check logic to have to run one time, and there are times I know I'll be sending in a guiRequest
        // i.e. after the go button is clicked.
        if(selectedStorageRequest !is null && storageManager !is null && !isUpdating) {
            if(guiRequest is null) {
                guiRequest = uiToRequest();
            }
            selectedStorageRequest.guiRequest = guiRequest;
            // writefln("Saving Gui Request: %s", selectedStorageRequest.serializeToJson());
            storageManager.updateRequest(selectedStorageRequest);
        }
    }

    private GuiRequest uiToRequest() {
        auto method = methodComboBox.getMethod();
        return new GuiRequest(urlEntry.getText(), method)
            .setQueryParams(queryValueList.getPairs())
            .setHeaders(headerValueList.getPairs())
            .setBodyType(bodyBox.getBodyType())
            .setBodyRaw(bodyBox.getRawBody())
            .setBodyPairs(bodyBox.getKVPairs())
            .setAuthType(authBox.getAuthType())
            .setAuth(authBox.getValuesAsKVPair())
            .setNotes(notes.getBuffer.getText)
            .setFilePath(bodyBox.getFilePath())
            .setFileKey(bodyBox.getFileKey());
    }

    // Check pending request is added to the glib idle callback loop.  See goButtonClicked
    bool checkPendingRequest() {
        try {
            receiveTimeout(dur!("msecs")(0), (shared GuiResponse response) {
                GuiResponse resp = cast(GuiResponse)response;
                resultsBox.updateForResponse(resp);
                if(storageManager !is null) {
                    storageManager.saveResponse(selectedStorageRequest.id, resp);
                }
                idleHandlerEnabled = false;
            });
            if(!idleHandlerEnabled) {
                return false;
            }
            // writeln("Waiting for data...");
        } catch (Throwable t) {
            return false;
        }
        return true;
    }

    public void onRemoved() {
        if(storageManager !is null) {
            writeln("Handling removed");
            storageManager.close();
        }
    }

}
