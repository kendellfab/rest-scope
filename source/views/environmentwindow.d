/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.environmentwindow;

import gtk.Window;
import gtk.Box;
import widgets.environmentscombobox;
import gtk.Button;
import models.environment;
import widgets.keyvaluelist;
import gtk.ScrolledWindow;
import data.restscopecolors;
import gtk.Popover;
import widgets.namenewbox;
import data.kvpair;
import gtk.ComboBox;
import std.stdio;
import widgets.baseenvironmentsactionbox;
import widgets.environmentstreeview;
import data.environmentstreestore;
import gtk.TreePath;
import gtk.TreeViewColumn;
import gtk.TreeView;
import gtk.Widget;
import gtk.TreeIter;
import callbacks.environmentmanagementcallback;
import widgets.childenvironmentsactionbox;
import gtk.Paned;

class EnvironmentWindow: Window {

    EnvironmentManagementCallback envManagementCallback;
    EnvironmentsTreeView envTreeView;
    EnvironmentsTreeStore envTreeStore;
    KeyValueList kvList;
    Popover addEnvPopover;
    Environment[] envs;
    Popover actionPopover;

    Environment selectedEnvironment;

    this(EnvironmentManagementCallback emc) {
        super("Environments");
        envManagementCallback = emc;

        setDefaultSize(960, 600);
        setTypeHint(WindowTypeHint.DIALOG);
        setDestroyWithParent(true);

        auto contentPaned = new Paned(GtkOrientation.HORIZONTAL);
        contentPaned.setMarginLeft(10);
        contentPaned.setMarginTop(5);
        contentPaned.setMarginRight(10);
        contentPaned.setMarginBottom(10);

        auto listWrapper = new Box(GtkOrientation.VERTICAL, 2);
        listWrapper.setMarginRight(5);

        auto buttonBox = new Box(GtkOrientation.HORIZONTAL, 2);
        buttonBox.setMarginTop(5);
        buttonBox.setMarginBottom(5);
        buttonBox.setMarginLeft(5);
        buttonBox.setSizeRequest(200, -1);

        auto addButton = new Button("list-add", GtkIconSize.MENU);

        addEnvPopover = new Popover(addButton);
        addEnvPopover.add(new NameNewBox(&addEnvironmentCallback, "Environment Name"));
        addButton.addOnClicked(delegate (Button _) {
            addEnvPopover.showAll();
        });
        buttonBox.add(addButton);
        listWrapper.add(buttonBox);
        contentPaned.pack1(listWrapper, false, false);

        envTreeStore = new EnvironmentsTreeStore();
        envTreeView = new EnvironmentsTreeView(envTreeStore);
        envTreeView.addOnRowActivated(&rowActivated);
        envTreeView.addOnButtonPress(&rowRightClick);
        listWrapper.add(envTreeView);

        auto sw = new ScrolledWindow();
        sw.setMarginLeft(5);
        sw.setHexpand(true);
        sw.setVexpand(true);

        kvList = new KeyValueList();
        kvList.setHexpand(true);
        kvList.setVexpand(true);

        kvList.pairRemovedCallback = &kvListUpdated;
        kvList.pairAddedCallback = &kvListUpdated;
        kvList.treeRowUpdated = &kvListUpdated;

        sw.add(kvList);
        contentPaned.pack2(sw, true, true);
       
        add(contentPaned);
    }

    bool rowRightClick(GdkEventButton* event, Widget _) {
        if(event.button == 3) {
            TreePath path;
            TreeViewColumn column;
            int cellX;
            int cellY;
            envTreeView.getPathAtPos(cast(int)event.x, cast(int)event.y, path, column, cellX, cellY);

            if(path.getDepth() == 1) {
                handleBaseRightClick(path, column);
            } else if(path.getDepth() == 2) {
                handleChildRichClick(path, column);
            }
            return true;
        }

        return false;
    }

    void handleBaseRightClick(TreePath path, TreeViewColumn column) {
        auto env = envs[path.getIndices[0]];

        GdkRectangle rect;
        envTreeView.getCellArea(path, column, rect);
        rect.y += 35;
        actionPopover = new Popover(envTreeView);
        actionPopover.setPointingTo(&rect);

        actionPopover.add(new BaseEnvironmentsActionBox(env, &doDeleteEnvironmentCallback, &doEditEnvironmentCallback, &doAddChildCallback));

        actionPopover.showAll();
    }

    void handleChildRichClick(TreePath path, TreeViewColumn column) {
        auto env = envs[path.getIndices[0]].children[path.getIndices[1]];

        GdkRectangle rect;
        envTreeView.getCellArea(path, column, rect);
        rect.y += 35;
        actionPopover = new Popover(envTreeView);
        actionPopover.setPointingTo(&rect);

        actionPopover.add(new ChildEnvironmentsActionBox(env, &doDeleteEnvironmentCallback, &doEditEnvironmentCallback));

        actionPopover.showAll();
    }

    void doDeleteEnvironmentCallback(Environment selectedEnvironment) {
        auto newEnvs = envManagementCallback.doDeleteEnvironment(selectedEnvironment);
        setEnvironments(newEnvs);
        actionPopover.hide();
        actionPopover.destroy();
    }

    void doEditEnvironmentCallback(Environment selectedEnvironment, string nameEdit) {
        selectedEnvironment.name = nameEdit;
        auto newEnvs = envManagementCallback.doUpdateEnvironment(selectedEnvironment);
        setEnvironments(newEnvs);
        actionPopover.hide();
        actionPopover.destroy();
    }

    void doAddChildCallback(Environment selectedEnvironment, string childName) {
        KVPair[] pairs;
        auto env = new Environment(0, childName, pairs, selectedEnvironment.id);
        auto newEnvs = envManagementCallback.doAddEnvironment(env);
        setEnvironments(newEnvs);
        actionPopover.hide();
        actionPopover.destroy();
    }

    void rowActivated(TreePath path, TreeViewColumn column, TreeView tree) {
        if(path.getDepth() == 1) {
            selectedEnvironment = this.envs[path.getIndices[0]];
            kvList.reset();
            kvList.updatePairs(selectedEnvironment.pairs);
        } else if(path.getDepth() == 2) {
            selectedEnvironment = this.envs[path.getIndices[0]].children[path.getIndices[1]];
            kvList.reset();
            kvList.updatePairs(selectedEnvironment.pairs);
        }
    }

    void addEnvironmentCallback(string name) {
        addEnvPopover.hide();
        KVPair[] pairs;
        auto env = new Environment(0, name, pairs);
        auto newEnvs = envManagementCallback.doAddEnvironment(env);
        setEnvironments(newEnvs);
    }

    void kvListUpdated() {
        selectedEnvironment.pairs = kvList.getPairs();
        envManagementCallback.doUpdateEnvironment(selectedEnvironment);
    }

    void setEnvironments(Environment[] envs) {
        this.envs = envs;
        envTreeStore.clear();
        if(envs !is null && envs.length > 0) {
            envTreeStore.addEnvironments(envs);

            envTreeView.activateFirst();
        }
    }
}