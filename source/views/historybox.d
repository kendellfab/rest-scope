/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.historybox;

import gtk.Box;
import gtk.Button;
import gtk.HSeparator;
import gtk.ListBox;
import widgets.colorlabel;
import data.restscopecolors;
import callbacks.historycallback;
import data.guiresponse;
import widgets.historylistboxitem;
import gtk.ScrolledWindow;
import std.stdio;
import gtk.Popover;

class HistoryBox: Box {

private:
    ListBox listBox;
    HistoryCallback historyCallback;

public:
    this(HistoryCallback historyCallback, Popover parentPopover) {
        super(GtkOrientation.VERTICAL, 2);
        setMarginLeft(10);
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);

        this.historyCallback = historyCallback;


        auto clearAllButton = new Button("Clear All");
        clearAllButton.addOnClicked = (Button _) {
            if(historyCallback.clearAllResponsesForRequest()) {
                listBox.removeAll();
            }
        };
        packStart(clearAllButton, false, false, 0);
        clearAllButton.setMarginBottom(4);

        auto sep = new HSeparator();
        packStart(sep, false, false, 0);

        auto sw = new ScrolledWindow();
        sw.setSizeRequest(400, 600);
        sw.setMarginTop(4);

        listBox = new ListBox();
        listBox.setPlaceholder(new ColorLabel("No responses", blue));
        listBox.setSelectionMode(GtkSelectionMode.SINGLE);
        sw.add(listBox);

        packStart(sw, false, false, 0);

        auto responses = historyCallback.listResponses();
        if(responses !is null) {
            foreach(GuiResponse resp; responses) {
                listBox.add(new HistoryListboxItem(resp, &doDeleteResponse));
            }

            listBox.addOnSelectedRowsChanged = (ListBox _) {
                auto row = cast(HistoryListboxItem) listBox.getSelectedRow();
                historyCallback.responseSelected(row.getResponse());
                parentPopover.hide();
            };
        }

    }

    void doDeleteResponse(GuiResponse response, HistoryListboxItem item) {
        if(historyCallback.deleteResponse(response)) {
            listBox.remove(item);
        }
    }
}