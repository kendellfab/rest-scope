/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.authbox;

import gtk.Box;
import widgets.authtypecombobox;
import gtk.Label;
import gtk.Entry;
import gtk.Grid;
import gtk.CheckButton;
import data.authtype;
import data.authtypeliststore;
import gtk.ComboBox;
import std.stdio;
import data.kvpair;
import callbacks.guirequestupdatecallback;
import gtk.Widget;
import gdk.Event;

class AuthBox: Box {

private:
    static string usernameLabel = "Username";
    static string passwordLabel = "Password";
    static string tokenLabel = "Token";
    static string prefixLabel = "Bearer prefix";
    AuthTypeComboBox authTypeComoboBox;
    Label nameTokenLabel;
    Entry nameTokenEntry;
    Label passwordBearerPrefixLabel;
    Entry passwordBearerPrefixEntry;
    CheckButton enabled;
    Grid grid;
    GuiRequestUpdateCallback guiRequestUpdateCallback;

    void initEntryGrid() {
        grid = new Grid();
        grid.setMarginTop(5);
        grid.setMarginStart(5);
        grid.setMarginEnd(5);
        grid.setMarginBottom(5);
        grid.setRowSpacing(10);
        grid.setColumnSpacing(5);
        nameTokenLabel = new Label(usernameLabel);
        nameTokenEntry = new Entry();
        nameTokenEntry.addOnFocusOut((Event e, Widget w) {
            if(guiRequestUpdateCallback !is null) {
                guiRequestUpdateCallback.requestUpdated();
            }
            return true;
        });
        nameTokenEntry.setHexpand(true);

        passwordBearerPrefixLabel = new Label(passwordLabel);
        passwordBearerPrefixEntry = new Entry();
        passwordBearerPrefixEntry.addOnFocusOut((Event e, Widget w) {
            if(guiRequestUpdateCallback !is null) {
                guiRequestUpdateCallback.requestUpdated();
            }
            return true;
        });
        passwordBearerPrefixEntry.setHexpand(true);

        grid.attach(nameTokenLabel, 0, 0 , 1, 1);
        grid.attach(nameTokenEntry, 1, 0, 1, 1);
        grid.attach(passwordBearerPrefixLabel, 0, 1, 1, 1);
        grid.attach(passwordBearerPrefixEntry, 1, 1, 1, 1);

        enabled = new CheckButton("Enabled");
        enabled.addOnToggled((_) {
            if(guiRequestUpdateCallback !is null) {
                guiRequestUpdateCallback.requestUpdated();
            }
        });

        grid.attach(enabled, 0, 2, 2, 1);

        add(grid);
        grid.showAll();
    }

public:
    this(GuiRequestUpdateCallback guiRequestUpdateCallback) {
        super(GtkOrientation.VERTICAL, 2);
        this.guiRequestUpdateCallback = guiRequestUpdateCallback;

        authTypeComoboBox = new AuthTypeComboBox();
        authTypeComoboBox.setMarginTop(2);
        authTypeComoboBox.setMarginStart(2);
        authTypeComoboBox.setMarginEnd(2);
        authTypeComoboBox.setMarginBottom(5);
        authTypeComoboBox.addOnChanged(&authTypeChanged);

        add(authTypeComoboBox);
    }

    void authTypeChanged(ComboBox _) {
        if(grid is null) {
            initEntryGrid();
        }
        auto chosenType = getAuthType();
        switch(chosenType) {
            case AuthType.None:
                grid.setVisible(false);
                enabled.setActive(false);
            break;
            case AuthType.Basic:
                grid.setVisible(true);
                nameTokenLabel.setText(usernameLabel);
                passwordBearerPrefixLabel.setText(passwordLabel);
                passwordBearerPrefixEntry.setPlaceholderText("");
                enabled.setActive(true);
            break;
            case AuthType.Bearer:
                grid.setVisible(true);
                nameTokenLabel.setText(tokenLabel);
                passwordBearerPrefixLabel.setText(prefixLabel);
                passwordBearerPrefixEntry.setPlaceholderText("Bearer if left empty");
                enabled.setActive(true);
            break;
            default:
            break;
        }
    }

    AuthType getAuthType() {
        const auto authType = authTypeComoboBox.getAuthType();
        if(authType == AuthTypeListStore.items[0]) {
            return AuthType.None;
        } else if (authType == AuthTypeListStore.items[1]) {
            return AuthType.Basic;
        } else {
            return AuthType.Bearer;
        }
    }

    void reset() {
        if(nameTokenEntry !is null) {
            nameTokenEntry.setText("");
        }
        if(passwordBearerPrefixEntry !is null) {
            passwordBearerPrefixEntry.setText("");
        }
        authTypeComoboBox.setActive(0);
    }

    KVPair getValuesAsKVPair() {
        auto key = nameTokenEntry is null ? "" : nameTokenEntry.getText();
        auto val = passwordBearerPrefixEntry is null ? "" : passwordBearerPrefixEntry.getText();
        auto active = enabled is null ? false : enabled.getActive();
        return new KVPair(key, val, active);
    }

    void updateAuth(AuthType at, KVPair auth) {
        if(at == AuthType.Basic) {
            authTypeComoboBox.setActive(1);
            nameTokenEntry.setText = auth.key;
            passwordBearerPrefixEntry.setText = auth.value;
            enabled.setActive = auth.active;
        } else if(at == AuthType.Bearer) {
            authTypeComoboBox.setActive(2);
            nameTokenEntry.setText = auth.key;
            passwordBearerPrefixEntry.setText = auth.value;
            enabled.setActive = auth.active;
        }
    }
}