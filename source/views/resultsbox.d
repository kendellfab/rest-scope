/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module views.resultsbox;

import gtk.Box;
import widgets.colorlabel;
import data.restscopecolors;
import gtk.Spinner;
import gtk.Expander;
import widgets.responseheaderstreeview;
import gsv.SourceView;
import gsv.SourceLanguage;
import gsv.SourceLanguageManager;
import gsv.SourceBuffer;
import gtk.ScrolledWindow;
import requests;
import std.algorithm;
import std.stdio;
import std.conv;
import data.guiresponse;
import std.json : parseJSON, JSONException;
import gtk.HSeparator;
import gtk.Label;
import gtk.Button;
import gtk.Clipboard;
import gdk.Atom;
import gtk.Entry;
import gtk.EditableIF;
import gsv.SourceSearchSettings;
import gsv.SourceSearchContext;
import gsv.SourceBuffer;
import gsv.SourceStyleSchemeManager;
import gobject.Signals;
import gdk.Event;
import std.format;
import gtk.TextIter;
import gsv.SourceStyleScheme;
import gtk.Notebook;
import gtk.Popover;
import views.historybox;
import callbacks.historycallback;

alias SaveSelectedSchemeCallback = void delegate(string schemeID);
private ResultsBox resultsBox;

class ResultsBox: Box {
private:
    static string awaitingRequest = "Awaiting Request";
    static string requestTime = "Request Time";
    static string requestSize = "Body Size";
    ColorLabel lblCode;
    ColorLabel lblTiming;
    ColorLabel lblSize;
    Label lblUrl;
    Spinner busySpinner;
    ResponseHeadersTreeView responseHeaders;
    SourceView svResponse;
    SourceBuffer sourceBuffer;
    Entry searchEntry;
    SourceSearchContext searchContext;
    SourceSearchSettings searchSettings;
    SourceLanguageManager slm;
    Label lblSearchResultsCount;
    ScrolledWindow scrolledWindow;
    Notebook displayNotebook;
    Popover historyPopover;

public:
    this(HistoryCallback historyCallback) {
        super(GtkOrientation.VERTICAL, 2);
        resultsBox = this;
        setMarginLeft(10);

        auto hudBox = new Box(GtkOrientation.HORIZONTAL, 2);

        lblCode = new ColorLabel(awaitingRequest, yellow);
        hudBox.packStart(lblCode, false, false, 0);

        lblTiming = new ColorLabel(requestTime, gray);
        lblTiming.setMarginLeft(5);
        hudBox.packStart(lblTiming, false, false, 0);

        lblSize = new ColorLabel(requestSize, gray);
        lblSize.setMarginLeft(5);
        hudBox.packStart(lblSize, false, false, 0);

        busySpinner = new Spinner();
        busySpinner.setHalign(GtkAlign.END);
        hudBox.packStart(busySpinner, false, false, 0);

        lblUrl = new Label("");
        lblUrl.setMarginLeft(5);
        lblUrl.setEllipsize(PangoEllipsizeMode.END);
        hudBox.packStart(lblUrl, false, false, 0);

        auto historyButton = new Button("appointment-soon-symbolic", GtkIconSize.MENU);
        hudBox.packEnd(historyButton, false, false, 0);
        historyButton.addOnClicked = (Button _) {
            historyPopover = new Popover(historyButton);
            historyPopover.add(new HistoryBox(historyCallback, historyPopover));
            historyPopover.showAll();
        };

        auto clipboardCopy = new Button("edit-copy", GtkIconSize.MENU);
        clipboardCopy.setTooltipText("Copy the request url to clipboard.");
        hudBox.packEnd(clipboardCopy, false, false, 0);
        clipboardCopy.addOnClicked = (Button _) {
            auto cb = Clipboard.get(intern("CLIPBOARD", true));
            cb.setText(lblUrl.getText, -1);
        };

        auto projSep = new HSeparator();
        projSep.setMarginTop(10);
        projSep.setMarginBottom(10);

        displayNotebook = new Notebook();
        displayNotebook.setVexpand(true);
        displayNotebook.setHexpand(true);

        responseHeaders = new ResponseHeadersTreeView();

        svResponse = new SourceView();
        svResponse.setShowLineNumbers(true);
        svResponse.setHexpand(true);
        svResponse.setVexpand(true);
        svResponse.setHighlightCurrentLine(true);

        searchSettings = new SourceSearchSettings();
        sourceBuffer = svResponse.getBuffer();
        searchContext = new SourceSearchContext(sourceBuffer, searchSettings);
        slm = new SourceLanguageManager();

        auto bodyBox = new Box(GtkOrientation.VERTICAL, 2);
        auto searchBox = new Box(GtkOrientation.HORIZONTAL, 2);
        searchBox.setMarginBottom(5);
        searchBox.setMarginTop(5);
        searchBox.setMarginStart(5);
        searchBox.setMarginEnd(5);
        searchEntry = new Entry();
        searchEntry.setPlaceholderText("Search");
        searchEntry.addOnChanged(&searchChanged);
        searchBox.packStart(searchEntry, false, false, 0);
        lblSearchResultsCount = new Label("");
        lblSearchResultsCount.setMarginLeft(15);
        searchBox.add(lblSearchResultsCount);

        scrolledWindow = new ScrolledWindow();
        scrolledWindow.add(svResponse);
        bodyBox.add(searchBox);
        bodyBox.add(scrolledWindow);

        displayNotebook.appendPage(bodyBox, "Body");
        displayNotebook.appendPage(responseHeaders, "Headers");

        packStart(hudBox, false, false, 0);
        packStart(projSep, false, true, 0);
        packStart(displayNotebook, true, true, 0);

    }

    void updateScheme(SourceStyleScheme scheme) {
        sourceBuffer.setStyleScheme(scheme);
    }

    void searchChanged(EditableIF _) {
        string searchText = searchEntry.getText();
        searchSettings.setSearchText(searchText);

        TextIter start;
        sourceBuffer.getIterAtOffset(start, 0);
        searchContext.forwardAsync(start, null, cast(GAsyncReadyCallback)&searchCallback, null);
        bool wrap;
        TextIter ms;
        TextIter me;
        searchContext.forward(start, ms, me, wrap);
    }

    void updateForResponse(GuiResponse rs) {
        if(rs.errorMessage !is null) {
            lblCode.setBgColor(red);
            lblCode.setText("Error");
            SourceBuffer sb = svResponse.getBuffer();
            sb.setText(rs.errorMessage);
            busySpinner.stop();
            SourceLanguageManager slm = new SourceLanguageManager();
            auto lang = slm.guessLanguage(null, "text/plain");
            sb.setLanguage(lang);
            return;
        }
        auto headers = rs.headers;
        // Splitting here as the content type string could be, text/html; utf-8.  The sourceview doesn't like the utf8
        auto keyContentType = "content-type";
        string contentType = "application/octet-stream";
        if(keyContentType in headers) {
            auto contentTypeHeader = headers["content-type"];
            auto parts = contentTypeHeader.findSplit("; ");
            contentType = parts[0];
        }

        lblUrl.setText = rs.url;

        if(rs.code >= 200 && rs.code < 300) {
            lblCode.setBgColor(green);
        } else if(rs.code >= 300 && rs.code < 400) {
            lblCode.setBgColor(blue);
        } else if(rs.code >= 400 && rs.code < 500) {
            lblCode.setBgColor(orange);
        } else {
            lblCode.setBgColor(red);
        }
        lblCode.setText(to!string(rs.code));

        lblTiming.setText(to!string(rs.recvTime + rs.connectTime) ~ " ms");
        lblSize.setText(rs.formatBytes());

        responseHeaders.addHeaders(headers);

        // Pretty print JSON output. Will fail on other output types, just bail if that's the case.
        try {
            // Some samples return text/html that is really json.  So we'er going to try to convert it here.
            auto jsonValue = parseJSON(rs.body);
            rs.body = jsonValue.toPrettyString();
            contentType = "application/json";
        } catch (JSONException exception) {

        }
        
        sourceBuffer.setText(rs.body);
        auto lang = slm.guessLanguage(null, contentType);
        sourceBuffer.setLanguage(lang);
        sourceBuffer.setHighlightSyntax(true);
        
        busySpinner.stop();
    }

    void setCodeLabelText(string text) {
        lblCode.setText(text);
    }

    void setBusySpinner(bool isBusy) {
        if(isBusy) {
            busySpinner.start();
        } else {
            busySpinner.stop();
        }
    }

    void reset() {
        lblCode.setText(awaitingRequest);
        lblCode.setBgColor(yellow);
        lblTiming.setText(requestTime);
        lblSize.setText(requestSize);
        lblUrl.setText("");
        responseHeaders.reset();
        sourceBuffer.setText = "";
        lblUrl.setText = "";
        searchEntry.setText = "";
        searchSettings.setSearchText = "";
        lblSearchResultsCount.setText = "";
    }

    void searchComplete() {
        int resultCount = searchContext.getOccurrencesCount();
        writefln("Search occurences -> %d", resultCount);
        if(resultCount > 0) {
            lblSearchResultsCount.setText(format("%d Occurences", resultCount));
        } else {
            lblSearchResultsCount.setText("");
        }
    }
}

extern(C) void searchCallback(GObject* src, GAsyncResult *res, void* userData) {
    resultsBox.searchComplete();
}