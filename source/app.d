/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import gtk.Main;
import restscope;
import std.conv;
import core.thread;
import std.concurrency;

void main(string[] args)
{
	thisTid();
	Main.init(args);

	auto restScope = new RestScope();

	restScope.run(args);

}

// Icon Link -> https://icons8.com/icons/set/centre-of-gravity