/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.responseheadersliststore;

import gtk.ListStore;
import gtk.TreeIter;

class ResponseHeadersListStore: ListStore {

    this() {
        super([GType.STRING, GType.STRING]);
    }

    public void addHeader(in string key, in string value) {
        TreeIter iter = createIter();
        setValue(iter, 0, key);
        setValue(iter, 1, value);
    }
}