/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.restscopecolors;

import gdk.Color;

Color white;
Color yellow;
Color green;
Color red;
Color gray;
Color blue;
Color orange;
Color darkGray;

shared static this() {
    white = new Color(255, 255, 255);
    yellow = new Color(252, 186, 3);
    green = new Color(12, 194, 51);
    red = new Color(194, 39, 12);
    gray = new Color(209, 209, 209);
    blue = new Color(79, 165, 227);
    orange = new Color(240, 156, 22);
    darkGray = new Color(50, 50, 50);
}