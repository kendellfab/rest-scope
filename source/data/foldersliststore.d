/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.foldersliststore;

import gtk.TreeStore;
import gtk.TreeIter;
import models.folder;
import models.storagerequest;
import gtk.TreePath;

class FoldersListStore: TreeStore {

    this() {
        super([GType.STRING, GType.STRING]);
    }

    void addFolder(Folder folder) {
        auto parentIter = createIter();
        setValue(parentIter, 0, folder.name);
        foreach(StorageRequest r; folder.requests) {
            auto childIter = append(parentIter);
            setValue(childIter, 1, r.name);
        }
    }

    void addRequestToFolder(string folder, StorageRequest request) {
        TreeIter treeIter;
        getIterFromString(treeIter, folder);

        auto childIter = append(treeIter);
        setValue(childIter, 1, request.name);
    }

    void updateFolderName(string name, string path) {
        TreeIter treeIter;
        getIterFromString(treeIter, path);
        setValue(treeIter, 0, name);
    }

    void updateRequestName(string name, string path) {
        TreeIter treeIter;
        getIterFromString(treeIter, path);
        setValue(treeIter, 1, name);
    }
}