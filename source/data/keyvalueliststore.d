/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.keyvalueliststore;

import gtk.ListStore;
private import gtk.TreeIter;
private import gtkc.gobjecttypes;

class KeyValueListStore: ListStore {

    this() {
        super([GType.STRING, GType.STRING, GType.INT]);
    }

    public void addPair(in string key, in string value, in bool active = true) {
        TreeIter iter = createIter();
        setValue(iter, 0, key);
        setValue(iter, 1, value);
        setValue(iter, 2, active ? 1 : 0);
    }
}