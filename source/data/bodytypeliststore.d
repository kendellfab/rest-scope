/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.bodytypeliststore;

import gtk.ListStore;

class BodyTypeListStore: ListStore {

    static string[] items = ["None", "Multipart", "Form URL Encoded", "JSON", "XML", "YAML", "Oher", "Binary File"];

    this() {
        super([GType.STRING]);

        foreach(string method; items) {
            auto iter = createIter();
            setValue(iter, 0, method);
        }
    }
}