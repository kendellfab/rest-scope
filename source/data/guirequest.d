/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.guirequest;

import data.kvpair;
import data.bodytype;
import data.authtype;

class GuiRequest {
    string url;
    string method;
    KVPair[] query;
    KVPair[] headers;
    BodyType bodyType;
    KVPair[] bodyPairs;
    string bodyRaw;
    AuthType authType;
    KVPair auth;
    string notes;
    string filePath;
    string fileKey;

    this() {

    }

    this(string url, string method) {
        this.url = url;
        this.method = method;
    }

    ~this() {
        
    }

    public GuiRequest setQueryParams(KVPair[] query) {
        this.query = query;
        return this;
    }

    public GuiRequest setHeaders(KVPair[] headers) {
        this.headers = headers;
        return this;
    }

    public GuiRequest setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
        return this;
    }

    public GuiRequest setBodyPairs(KVPair[] bodyPairs) {
        this.bodyPairs = bodyPairs;
        return this;
    }

    public GuiRequest setBodyRaw(string bodyRaw) {
        this.bodyRaw = bodyRaw;
        return this;
    }

    public GuiRequest setAuthType(AuthType at) {
        this.authType = at;
        return this;
    }

    public GuiRequest setAuth(KVPair pair) {
        this.auth = pair;
        return this;
    }

    public GuiRequest setNotes(string notes) {
        this.notes = notes;
        return this;
    }

    public GuiRequest setFilePath(string path) {
        this.filePath = path;
        return this;
    }

    public GuiRequest setFileKey(string key) {
        this.fileKey = key;
        return this;
    }

    public string getUrl() {
        return url;
    }

    public string getMethod() {
        return method;
    }

    public KVPair[] getQueryParams() {
        return query;
    }

    public KVPair[] getHeaders() {
        return headers;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public KVPair[] getBodyPairs() {
        return bodyPairs;
    }

    public string getBodyRaw() {
        return bodyRaw;
    }

    public AuthType getAuthType() {
        return authType;
    }

    public KVPair getAuth() {
        return auth;
    }

    public string getNotes() {
        return notes;
    }

    public string getFilePath() {
        return filePath;
    }

    public string getFileKey() {
        return fileKey;
    }
}