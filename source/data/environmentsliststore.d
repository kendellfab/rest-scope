/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.environmentsliststore;

import gtk.ListStore;
import models.environment;
import gtk.TreeIter;

class EnvironmentsListStore: ListStore {
    this() {
        super([GType.STRING]);
    }

    void addEnvironment(Environment env) {
        TreeIter iter = createIter();
        setValue(iter, 0, env.name);
    }

    void addChildEnvironment(Environment env) {
        TreeIter iter = createIter();
        setValue(iter, 0, "\t" ~ env.name);
    }
}
