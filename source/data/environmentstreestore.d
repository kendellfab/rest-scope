/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.environmentstreestore;

import gtk.TreeStore;
import models.environment;

class EnvironmentsTreeStore: TreeStore {
    
    this() {
        super([GType.STRING, GType.STRING]);
    }

    void addBase(Environment env) {
        auto parentIter = createIter();
        setValue(parentIter, 0, env.name);
    }

    void addEnvironments(Environment[] envs) {
        foreach(Environment env; envs) {
            auto parentIter = createIter();
            setValue(parentIter, 0, env.name);
            foreach(Environment child; env.children) {
                auto childIter = append(parentIter);
                setValue(childIter, 1, child.name);
            }
        }
    }
}
