/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.guiresponse;

import std.datetime;
import asdf;
import std.format;

class GuiResponse {
public:
    int code;
    long connectTime;
    long recvTime;
    string[string] headers;
    string body;
    string url;
    string errorMessage;
    long size;

    @serializationIgnore long responseId;
    @serializationIgnore SysTime at;

    this() {

    }

    ~this() {
        
    }

    string formatBytes() {
        const long unit = 1024;
        if(size < unit) {
            return format("%d B", size);
        }

        long div = unit;
        long exp = 0;
        for(int i = cast(int)(size / unit); i >= unit; i /= unit) {
            div *= unit;
            exp++;
        }

        return format("%.1f %ciB", cast(float)size / cast(float)div, "KMGTPE"[exp]);
    }
}