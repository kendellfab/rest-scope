/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.authtypeliststore;

import gtk.ListStore;

class AuthTypeListStore: ListStore {
    static string[] items = ["None", "Basic", "Bearer"];

    this() {
        super([GType.STRING]);

        foreach(string auth; items) {
            auto iter = createIter();
            setValue(iter, 0, auth);
        }
    }
}