/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module data.projectsliststore;

import gtk.ListStore;
import models.project;
import gtk.TreeIter;

class ProjectsListStore: ListStore {

    this() {
        super([GType.STRING]);
    }

    void addProject(Project project) {
        TreeIter iter = createIter();
        setValue(iter, 0, project.name);
    }

    void updateProjectName(string projectName, TreeIter iter) {
        setValue(iter, 0, projectName);
    }
}

