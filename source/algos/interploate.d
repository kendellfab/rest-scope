/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module algos.interploate;

import std.algorithm;
import std.string;
import data.kvpair;
import std.uri;

public string interpolateParameter(string parameter, string[string] activeEnv) {
    if(parameter.canFind("{{") && parameter.canFind("}}")) {
        parameter = parameter.replace("{{", "");
        parameter = parameter.replace("}}", "");
        if(parameter in activeEnv) {
            return activeEnv[parameter];
        } else {
            return "";
        }
    } else {
        return parameter;
    }
}

public string encodePairs(KVPair[] pairs, string[string] activeEnv) {
    string result = "";
    foreach(KVPair p; pairs) {
        if(p.active) {
            result ~= p.key ~ "=" ~ interpolateParameter(p.value, activeEnv).encode ~ "&";
        }
    }
    if(result is "") {
        return "";
    }
    return result[0..result.length-1];
}