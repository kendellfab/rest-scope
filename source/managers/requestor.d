/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module managers.requestor;

import data.guirequest;
import std.concurrency;
import std.stdio;
import std;
import std.algorithm;
import data.kvpair;
import requests;
import asdf;
import data.bodytype;
import data.authtype;
import std.datetime;
import data.guiresponse;
import glib.Unicode;
import std.string;
import std.stdio;
import std.array;
import algos.interploate;
import std.datetime.systime;
import std.path;

class Requestor {
    private GuiRequest guiRequest;
    private string[string] activeEnv;

    public this(GuiRequest guiRequest, string[string] activeEnv) {
        this.guiRequest = guiRequest;
        this.activeEnv = activeEnv;
    }

    ~this() {
        
    }

    public void doRequest() {
        spawn(&makeRequest, cast(shared) guiRequest, cast(shared) activeEnv);
    }
}

private void makeRequest(shared GuiRequest guiRequest, shared string[string] inEnv) {
    writeln("---------------------");
    GuiRequest gr = cast(GuiRequest) guiRequest;
    string[string] activeEnv = cast(string[string])inEnv;

    writeln(gr.serializeToJson());
    writeln(activeEnv.serializeToJson());

    // I need to sanitize incoming url, as it seems to be unable to resolve with query string.
    string url = gr.getUrl();

    if(activeEnv != null) {
        import mustache;
        alias Mustache = MustacheEngine!(string);

        Mustache m;
        auto context = new Mustache.Context;
        foreach(string key; activeEnv.byKey()) {
            context[key] = activeEnv[key];
        }
        
        url = m.renderString(url, context);
    }

    string[string] params;
    foreach(KVPair p; gr.getQueryParams()) {
        if(p.active) {
            params[p.key] = interpolateParameter(p.value, activeEnv);
        }
    }
    string[string] headers;
    headers["user-agent"] = "RestScope";
    foreach(KVPair p; gr.getHeaders()) {
        if(p.active) {
            headers[p.key] = interpolateParameter(p.value, activeEnv);
        }
    }

    Request rq = Request();

    auto authType = gr.getAuthType();
    auto authValues = gr.getAuth();
    if(authType == AuthType.Basic) {
        if(authValues.active) {
            rq.authenticator = new BasicAuthentication(interpolateParameter(authValues.key, activeEnv), interpolateParameter(authValues.value, activeEnv));
        }
    } else if(authType == AuthType.Bearer) {
        if(authValues.active) {
            headers["Authorization"] = (authValues.value is null ? "Bearer " : authValues.value) ~ interpolateParameter(authValues.key, activeEnv);
        }
    }
    rq.addHeaders(headers);
    rq.verbosity = 2;

    try {
        switch(gr.getMethod()) {
            case "POST":
                executeBody("POST", url, activeEnv, gr, rq);
                break;
            case "PUT":
                executeBody("PUT", url, activeEnv, gr, rq);
                break;
            case "DELETE":
                sendResponse(rq.deleteRequest(url, params), null, activeEnv);
                break;
            default:
                sendResponse(rq.get(url, params), gr.getQueryParams(), activeEnv);
            break;
        }
    } catch(ConnectError e) {
        writeln("--- Caught exception ---");
        writeln(e);
        GuiResponse guiResponse = new GuiResponse();
        guiResponse.errorMessage = cast(string)e.message;
        ownerTid.send(cast(shared)guiResponse);
    } catch(Exception e) {
        writeln("--- Exception ---");
        writeln(e);
        GuiResponse guiResponse = new GuiResponse();
        guiResponse.errorMessage = cast(string)e.message;
        ownerTid.send(cast(shared)guiResponse);
    }

    writeln("---------------------");
}

private void executeBody(string method, string url, string[string] activeEnv, GuiRequest gr, Request rq) {
    switch(gr.getBodyType()) {
        case BodyType.Multipart:
            MultipartForm form;
            foreach(KVPair pair; gr.getBodyPairs()) {
                if(pair.active) {
                    form.add(formData(pair.key, interpolateParameter(pair.value, activeEnv)));
                }
            }
            sendResponse(rq.execute(method, url, form), null, activeEnv);
        break;
        case BodyType.Form:
            string[string] form;
            foreach(KVPair pair; gr.getBodyPairs()) {
                if(pair.active) {
                    form[pair.key] = interpolateParameter(pair.value, activeEnv);
                }
            }
            sendResponse(rq.execute(method, url, aa2params(form)), null, activeEnv);
        break;
        case BodyType.JSON:
            sendResponse(rq.execute!string(method, url, gr.getBodyRaw(), "application/json"), null, activeEnv);
        break;
        case BodyType.XML:
            sendResponse(rq.execute!string(method, url, gr.getBodyRaw(), "application/xml"), null, activeEnv);
        break;
        case BodyType.YAML:
            sendResponse(rq.execute!string(method, url, gr.getBodyRaw(), "application/yaml"), null, activeEnv);
        break;
        case BodyType.OTHER:
            sendResponse(rq.execute!string(method, url, gr.getBodyRaw()), null, activeEnv);
        break;
        case BodyType.File:
            writeln(gr.filePath);
            MultipartForm form;
            auto src = File(gr.filePath);
            form.add(formData(gr.fileKey is null ? "file" : gr.fileKey, File(gr.filePath), ["filename": baseName(gr.filePath), "Content-Type": "application/octet-stream"]));
            sendResponse(rq.execute(method, url, form), null, activeEnv);
        break;
        default:

    }
}

private void sendResponse(Response response, KVPair[] query = null, string[string] activeEnv = null) {
    auto stat = response.getStats();
    GuiResponse guiResponse = new GuiResponse();

    guiResponse.code = response.code;
    guiResponse.connectTime = stat.connectTime.total!"msecs";
    guiResponse.recvTime = stat.recvTime.total!"msecs";
    guiResponse.headers = response.responseHeaders;
    guiResponse.body = cast(string)response.responseBody.data;
    guiResponse.size = guiResponse.body.length;
    
    if(guiResponse.body !is null) {
        guiResponse.body = Unicode.utf8MakeValid(guiResponse.body, -1);
    }
    string url = response.uri.recalc_uri();
    if(query !is null) {
        string encoded = encodePairs(query, activeEnv);
        long index = indexOf(url, '?');
        if(index > 0) {
            url ~= "&" ~ encoded;
        } else {
            if(encoded !is "") {
                url ~= "?" ~ encoded;
            }
        }
    }
    
    guiResponse.url = url;

    ownerTid.send(cast(shared)guiResponse);

}