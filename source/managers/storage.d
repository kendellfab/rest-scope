/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module managers.storage;

import models.project;
import models.folder;
import models.storagerequest;
import asdf;
import data.guirequest;
import models.environment;
import data.kvpair;
import std.stdio;
import std.algorithm;
import ddbc;
import std.variant;
import std.datetime.systime;
import data.guiresponse;

class StorageManager {

private:
    string dsn;
    DataSource cPool;

public:
    this(string path) {
        dsn = "sqlite:" ~ path;

        cPool = createConnectionPool(dsn);

        auto conn = cPool.getConnection();
        scope(exit) conn.close();
        auto pragmaStmt = conn.prepareStatement("PRAGMA journal_mode = memory;");
        scope(exit) pragmaStmt.close();
        pragmaStmt.executeUpdate();

        auto projectStmt = conn.prepareStatement(`create table if not exists project(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL
        );`);
        scope(exit) projectStmt.close();
        projectStmt.executeUpdate();

        auto folderStmt = conn.prepareStatement(`create table if not exists folder(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            project_id INTEGER
        );`);
        scope(exit) folderStmt.close();
        folderStmt.executeUpdate();

        auto reqStmt = conn.prepareStatement(`create table if not exists storagerequest(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            folder_id INTEGER,
            guirequest TEXT
        );`);
        scope(exit) reqStmt.close();
        reqStmt.executeUpdate();

        auto envStmt = conn.prepareStatement(`create table if not exists environment(
            id INTEGER PRIMARY KEY,
            name TEXT NOT NULL,
            pairs TEXT NOT NULL,
            parentid INTEGER
        );`);
        scope(exit) envStmt.close();
        envStmt.executeUpdate();

        auto responseStmt = conn.prepareStatement(`create table if not exists response(
            id INTEGER PRIMARY KEY,
            request_id INTEGER,
            at DATETIME,
            response TEXT,
            code INTEGER,
            duration INTEGER
        );`);
        scope(exit) responseStmt.close();
        responseStmt.executeUpdate();

        try {
            auto parentIdStmt = conn.prepareStatement(`alter table environment add column parentid integer default 0;`);
            scope(exit) parentIdStmt.close();
            parentIdStmt.executeUpdate();
        } catch(Exception e) {
            if(!e.msg.canFind("duplicate column name")) {
                throw e;
            }
        }
    }

    void close() {
        
    }

    long addProject(Project project) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("INSERT INTO project(name) VALUES(?);");
        scope(exit) stmt.close();
        stmt.setString(1, project.name);
        Variant res;
        stmt.executeUpdate(res);
        
        return res.coerce!long();
    }

    void updateProject(Project project) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("UPDATE project SET name = ? WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setString(1, project.name);
        stmt.setLong(2, project.id);
        stmt.executeUpdate();

    }

    void deleteProject(Project project) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM project WHERE id = :id;");
        scope(exit) stmt.close();
        stmt.setLong(1, project.id);
        stmt.executeUpdate();

    }

    Project[] listProjects() {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        Project[] projects;
        auto stmt = conn.prepareStatement("SELECT id, name FROM project;");
        scope(exit) stmt.close();
        auto results = stmt.executeQuery();
        while(results.next()) {
            long id = results.getLong(1);
            string name = results.getString(2);
            projects ~= new Project(id, name);
        }

        return projects;
    }

    long addFolder(Folder folder) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("INSERT INTO folder(name, project_id) VALUES(?, ?);");
        scope(exit) stmt.close();
        stmt.setString(1, folder.name);
        stmt.setLong(2, folder.projectId);
        Variant longId;
        stmt.executeUpdate(longId);

        return longId.coerce!long();
    }

    void updateFolder(Folder folder) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("UPDATE folder SET name = ? WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setString(1, folder.name);
        stmt.setLong(2, folder.id);
        stmt.executeUpdate();

    }

    void deleteFolder(Folder folder) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM folder WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, folder.id);
        stmt.executeUpdate();

        deleteRequestsByFolder(folder.id);
    }

    Folder[] listFolders(long projectId) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        Folder[] folders;
        auto stmt = conn.prepareStatement("SELECT id, name FROM folder WHERE project_id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, projectId);
        auto results = stmt.executeQuery();
        while(results.next()) {
            long id = results.getLong(1);
            string name = results.getString(2);
            auto folder = new Folder(id, name, projectId);
            folder.requests = listStorageRequests(folder.id);
            folders ~= folder;
        }

        return folders;
    }

    long addRequest(StorageRequest storageRequest) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("INSERT INTO storagerequest(name, folder_id, guirequest) VALUES(?, ?, ?);");
        scope(exit) stmt.close();
        stmt.setString(1, storageRequest.name);
        stmt.setLong(2, storageRequest.folderId);
        stmt.setString(3, storageRequest.guiRequest.serializeToJson());
        Variant lastId;
        stmt.executeUpdate(lastId);

        return lastId.coerce!long();
    }

    void deleteRequest(StorageRequest storageRequest) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE from storagerequest WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, storageRequest.id);
        stmt.executeUpdate();

    }

    void deleteRequestsByFolder(long folderId) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM storagerequest WHERE folder_id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, folderId);
        stmt.executeUpdate();

    }

    void updateRequest(StorageRequest req) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("UPDATE storagerequest SET name = ?, guirequest = ? WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setString(1, req.name);
        stmt.setString(2, req.guiRequest.serializeToJson());
        stmt.setLong(3, req.id);
        stmt.executeUpdate();

    }

    GuiRequest loadGuiRequest(long storageRequestId) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("SELECT guirequest FROM storagerequest WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, storageRequestId);

        auto result = stmt.executeQuery();
        result.next();

        string grString = result.getString(1);
        if(grString is null) {
            return null;
        }
        return grString.deserialize!GuiRequest;
    }

    StorageRequest[] listStorageRequests(long folderId) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        StorageRequest[] requests;
        auto stmt = conn.prepareStatement("SELECT id, name, guirequest FROM storagerequest WHERE folder_id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, folderId);
        auto results = stmt.executeQuery();
        while(results.next()) {
            long id = results.getLong(1);
            string name = results.getString(2);
            requests ~= new StorageRequest(id, name, folderId, null);
        }

        return requests;
    }

    long addEnvironment(Environment env) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("INSERT INTO environment(name, pairs, parentid) VALUES(?, ?, ?);");
        scope(exit) stmt.close();
        stmt.setString(1, env.name);
        stmt.setString(2, env.pairs.serializeToJson());
        stmt.setLong(3, env.parentId);
        Variant lastId;
        stmt.executeUpdate(lastId);

        return lastId.coerce!long();
    }

    void updateEnvironment(Environment env) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("UPDATE environment SET name = ?, pairs = ? WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setString(1, env.name);
        stmt.setString(2, env.pairs.serializeToJson());
        stmt.setLong(3, env.id);
        stmt.executeUpdate();

    }

    Environment[] listHierarchy() {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        Environment[] parents;
        auto stmt = conn.prepareStatement("SELECT * FROM environment WHERE parentid = 0;");
        scope(exit) stmt.close();
        auto results = stmt.executeQuery();
        while(results.next()) {
            long id = results.getLong(1);
            string name = results.getString(2);
            string pairs = results.getString(3);
            auto children = listChildren(id);
            auto env = new Environment(id, name, pairs.deserialize!(KVPair[]));
            env.children = children;
            parents ~= env;
        }

        return parents;
    }

    Environment[] listChildren(long parentId) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        Environment[] children;
        auto stmt = conn.prepareStatement("SELECT * FROM environment WHERE parentid = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, parentId);
        auto results = stmt.executeQuery();
        while(results.next()) {
            long id = results.getLong(1);
            string name = results.getString(2);
            string pairs = results.getString(3);
            children ~= new Environment(id, name, pairs.deserialize!(KVPair[]), parentId);
        }

        return children;
    }

    void deleteEnvironment(Environment env) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM environment WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, env.id);
        stmt.executeUpdate();

    }

    Environment loadEnvironment(long id) {
        auto conn = cPool.getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("SELECT * FROM environment WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, id);
        auto res = stmt.executeQuery();
        res.next();
        
        auto name = res.getString(2);
        auto pairs = res.getString(3);
        auto parentId = res.getLong(4);
        Environment env = new Environment(id, name, pairs.deserialize!(KVPair[]), parentId);
        return env;
    }

    long saveResponse(long requestId, GuiResponse response) {
        auto conn = getConnection();
        scope(exit) conn.close();
        string asJson = response.serializeToJson();
        auto now = Clock.currTime();

        auto stmt = conn.prepareStatement("INSERT INTO response(request_id, at, response, code, duration) VALUES(?, ?, ?, ?, ?);");
        scope(exit) stmt.close();
        stmt.setLong(1, requestId);
        stmt.setString(2, now.toISOExtString());
        stmt.setString(3, asJson);
        stmt.setInt(4, response.code);
        stmt.setLong(5, response.connectTime + response.recvTime);

        Variant lastId;
        stmt.executeUpdate(lastId);

        return lastId.coerce!long();
    }

    GuiResponse getLatestResponse(long requestId) {
        auto conn = getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("SELECT response FROM response WHERE request_id = ? ORDER BY at DESC LIMIT 1;");
        scope(exit) stmt.close();
        stmt.setLong(1, requestId);
        auto res = stmt.executeQuery();
        if(res.next()) {
            scope(exit) res.close();
            string asJson = res.getString(1);
            return asJson.deserialize!(GuiResponse);
        }
        return null;
    }

    GuiResponse[] listResponsesForRequest(long requestId) {
        auto conn = getConnection();
        scope(exit) conn.close();

        GuiResponse[] responses;
        auto stmt = conn.prepareStatement("SELECT id, at, response FROM response WHERE request_id = ? ORDER BY at DESC;");
        scope(exit) stmt.close();
        stmt.setLong(1, requestId);
        auto res = stmt.executeQuery();
        while(res.next()) {
            long id = res.getLong(1);
            string at = res.getString(2);
            string response = res.getString(3);

            auto r = response.deserialize!(GuiResponse);
            r.responseId = id;
            r.at = SysTime.fromISOExtString(at);

            responses ~= r;
        }

        return responses;
    }

    void deleteResponse(GuiResponse response) {
        auto conn = getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM response WHERE id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, response.responseId);
        stmt.executeUpdate();
    }

    void clearAllResponsesForRequest(long requestId) {
        auto conn = getConnection();
        scope(exit) conn.close();

        auto stmt = conn.prepareStatement("DELETE FROM response WHERE request_id = ?;");
        scope(exit) stmt.close();
        stmt.setLong(1, requestId);

        stmt.executeUpdate();
    }

    Connection getConnection() {
        auto conn = cPool.getConnection();
        auto pmemStmt = conn.prepareStatement("PRAGMA journal_mode = MEMORY;");
        scope(exit) pmemStmt.close();
        pmemStmt.executeUpdate();
        return conn;
    }
}