/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module settings.manager;

interface SettingsManager {
    public string getLastDB();
    public void setLastDB(string value);
    public string getSchemeID();
    public void setSchemeID(string schemeID);
    public long getSelectedEnvironmentID();
    public void setSelectedEnvironmentID(long envID);
}