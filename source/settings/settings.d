/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module settings.settings;

class Settings {
package:
    public string lastDB;
    public string schemeID;
    public long selectedEnvironmentID;

public:
    this() {

    }
}