/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module settings.filemanager;

import settings.manager;
import settings.settings;
import std.stdio;
import std.file;
static import std.path;
import std.path;
import asdf;
static import std.process;
import glib.Util;

class FileManager: SettingsManager {

private:
    string rootPath;
    string settingsPath;
    Settings settings;


public:

    this() {
        auto rawPath = Util.getUserConfigDir();
        writefln("Settings base directory: %s", rawPath);
        rootPath = buildPath(rawPath, "restscope");
        if(!exists(rootPath)) {
            mkdir(rootPath);
        }
        settingsPath = buildPath(rootPath, "settings.json");
        if(exists(settingsPath)) {
            auto content = readText(settingsPath);
            settings = content.deserialize!Settings;
        } else {
            settings = new Settings();
        }

    }

    public string getLastDB() {
        return settings.lastDB is null ? "" : settings.lastDB;
    }

    public void setLastDB(string value) {
        settings.lastDB = value;
        saveSettings();
    }

    public string getSchemeID() {
        return settings.schemeID is null ? "" : settings.schemeID;
    }

    public void setSchemeID(string schemeID) {
        settings.schemeID = schemeID;
        saveSettings();
    }

    public long getSelectedEnvironmentID() {
        return settings.selectedEnvironmentID;
    }

    public void setSelectedEnvironmentID(long envID) {
        settings.selectedEnvironmentID = envID;
        saveSettings();
    }

    private void saveSettings() {
        auto data = settings.serializeToJsonPretty();
        toFile(data, settingsPath);
    }

}