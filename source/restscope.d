/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module restscope;

import gtk.Application;
import gio.Application : GApplication = Application;
import views.appwindow;
import settings.filemanager;
import gtk.Window;

class RestScope: Application {

private:
    MainWindow mainWindow;

    void onAppActivate(GApplication app) {

        auto settingsManager = new FileManager();

        this.mainWindow = new MainWindow(this, settingsManager);
        this.mainWindow.showAll();
    }

    void onWindowRemoved(Window w, Application a) {
        this.mainWindow.onRemoved();
    }

public:
    this() {
        super("com.gitlab.kendellfab.restscope", ApplicationFlags.FLAGS_NONE);
        this.addOnActivate(&onAppActivate);
        this.addOnWindowRemoved(&onWindowRemoved);
    }
}