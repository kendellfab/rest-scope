/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.keyvaluelist;

import gtk.Box;
import gtk.Button;
import gtk.TreeIter;
import std.stdio;
import data.kvpair;
import gtk.Widget;
import data.keyvalueliststore;
import widgets.keyvaluetreeview;
import widgets.keyvalueinputbox;
import gtk.Image;
import gtk.Popover;
import callbacks.guirequestupdatecallback;

alias PairAddedCallback = void delegate();
alias PairRemovedCallback = void delegate();
alias TreeRowUpdated = void delegate();

class KeyValueList: Box {

    private Button addMore;
    private Box listBox;
    private Popover addKVPairPopover;

    private KeyValueListStore store;
    private KeyValueTreeView treeView;

    private GuiRequestUpdateCallback guiRequestUpdateCallback;

    PairAddedCallback pairAddedCallback;
    PairRemovedCallback pairRemovedCallback;
    TreeRowUpdated treeRowUpdated;

    public this() {
        super(GtkOrientation.VERTICAL, 2);

        auto buttonBox = new Box(GtkOrientation.HORIZONTAL, 2);
        buttonBox.setMarginTop(5);
        buttonBox.setMarginBottom(5);
        buttonBox.setMarginLeft(5);
        auto addButton = new Button("list-add", GtkIconSize.MENU);
        addButton.addOnClicked(&addKVPair);
        buttonBox.add(addButton);

        addKVPairPopover = new Popover(addButton);
        addKVPairPopover.add(new KeyValuePairInputBox(&doAddPair));

        auto removeImage = new Image("list-remove", GtkIconSize.MENU);
        auto removeButton = new Button();
        removeButton.addOnClicked(&removeKVPair);
        removeButton.add(removeImage);
        buttonBox.add(removeButton);

        add(buttonBox);

        store = new KeyValueListStore();
        treeView = new KeyValueTreeView(store);
        treeView.itemUpdatedCallback = &itemUpdatedCallback;
        treeView.setVexpand(true);

        add(treeView);
    }

    void itemUpdatedCallback() {
        if(treeRowUpdated !is null) {
            treeRowUpdated();
        }
        if(guiRequestUpdateCallback !is null) {
            guiRequestUpdateCallback.requestUpdated();
        }
    }

    private void addKVPair(Button _) {
        addKVPairPopover.showAll();
    }

    protected void doAddPair(string key, string value) {
        store.addPair(key, value, true);
        addKVPairPopover.hide();
        if(pairAddedCallback !is null) {
            pairAddedCallback();
        }
        if(guiRequestUpdateCallback !is null) {
            guiRequestUpdateCallback.requestUpdated();
        }
    }

    protected void removeKVPair(Button _) {
        if(auto iter = treeView.getSelectedIter()) {
            store.remove(iter);
            if(pairRemovedCallback !is null) {
                pairRemovedCallback();
            }
            if(guiRequestUpdateCallback !is null) {
                guiRequestUpdateCallback.requestUpdated();
            }
        }
    }

    KVPair[] getPairs() {
        KVPair[] res;
        
        auto iter = new TreeIter();
        iter.setModel(store);

        if(store.getIterFirst(iter)) {
            do {
                string key = store.getValueString(iter, 0);
                string value = store.getValueString(iter, 1);
                int active = store.getValueInt(iter, 2);

                res ~= new KVPair(key, value, active == 1);
            } while(store.iterNext(iter));
        }

        return res;
    }

    public void reset() {
        store.clear();
    }

    public void updatePairs(KVPair[] pairs) {
        foreach(KVPair p; pairs) {
            store.addPair(p.key, p.value, p.active);
        }
    }

    public void setGuiRequestUpdateCallback(GuiRequestUpdateCallback guiRequestUpdateCallback) {
        this.guiRequestUpdateCallback = guiRequestUpdateCallback;
    }
}