/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.folderactionbox;

import gtk.Box;
import gtk.Grid;
import gtk.Label;
import gtk.Entry;
import gtk.Button;
import gtk.HSeparator;

alias DoAddName = void delegate(string name);
alias RequestFolderDelete = void delegate();
alias DoEditFolder = void delegate(string folderName);

class FolderActionBox: Grid {
private:
    Label nameLabel;
    Entry nameEntry;
    Label editLabel;
    Entry editEntry;

public:
    this(DoAddName doAddName, RequestFolderDelete requestDelete, DoEditFolder doEditFolder, string lablelText, string editTitle, string folderName) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);

        nameLabel = new Label(lablelText);
        nameEntry = new Entry();
        nameEntry.setHexpand(true);

        setRowSpacing(10);
        setColumnSpacing(5);

        attach(nameLabel, 0, 0, 1, 1);
        attach(nameEntry, 1, 0, 1, 1);

        auto doAdd = new Button("Add Request");
        attach(doAdd, 0, 1, 2, 1);

        doAdd.addOnClicked(delegate void(Button _) {
            doAddName(nameEntry.getText());
            nameEntry.setText("");
        });

        auto hSep = new HSeparator();
        hSep.setMarginTop(10);
        hSep.setMarginBottom(10);
        attach(hSep, 0, 2, 2, 1);

        editLabel = new Label(editTitle);
        editEntry = new Entry();
        editEntry.setPlaceholderText(folderName);
        editEntry.setHexpand(true);
        auto doEditButton = new Button("Edit Name");
        doEditButton.addOnClicked(delegate void(Button _) {
            doEditFolder(editEntry.getText());
            editEntry.setText("");
        });


        attach(editLabel, 0, 3, 1, 1);
        attach(editEntry, 1, 3, 1, 1);
        attach(doEditButton, 0, 4, 2, 1);

        auto secondSep = new HSeparator();
        secondSep.setMarginTop(10);
        secondSep.setMarginBottom(10);
        attach(secondSep, 0, 5, 2, 1);

        auto requestDel = new Button("Delete");
        attach(requestDel, 0, 6, 2, 1);
        requestDel.addOnClicked(delegate void(Button ) {
            requestDelete();
        });
    }
}