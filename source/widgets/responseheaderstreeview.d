/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.responseheaderstreeview;

import gtk.TreeView;
import data.responseheadersliststore;
import gtk.CellRendererText;
import gtk.TreeViewColumn;
import std.algorithm;

class ResponseHeadersTreeView: TreeView {

    ResponseHeadersListStore listStore;

    this() {
        super();
        listStore = new ResponseHeadersListStore();
        setModel(listStore);

        auto keyRenderer = new CellRendererText();
        auto keyColumn = new TreeViewColumn("Name", keyRenderer, "text", 0);
        keyColumn.setResizable(true);
        appendColumn(keyColumn);

        auto valueRenderer = new CellRendererText();
        auto valueColumn = new TreeViewColumn("Value", valueRenderer, "text", 1);
        valueColumn.setResizable(true);
        appendColumn(valueColumn);
    }

    public void addHeaders(string[string] headers) {
        listStore.clear();
        foreach(string key; headers.keys.sort) {
            listStore.addHeader(key, headers[key]);
        }
    }

    public void reset() {
        listStore.clear();
    }
}