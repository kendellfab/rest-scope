/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.keyvalueinputbox;

import gtk.Box;
import gtk.Grid;
import gtk.Label;
import gtk.Entry;
import gtk.Button;

alias DoAddPair = void delegate(string key, string value);

class KeyValuePairInputBox: Grid {

    private Entry key;
    private Entry value;
    private Button doAdd;

    this(DoAddPair doAddPair) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);

        setRowSpacing(10);
        setColumnSpacing(5);

        auto keyLabel = new Label("Key");
        key = new Entry();
        key.setHexpand(true);
        attach(keyLabel, 0, 0, 1, 1);
        attach(key, 1, 0, 1, 1);
        

        auto valueLabel = new Label("Value");
        value = new Entry();
        value.setHexpand(true);
        attach(valueLabel, 0, 1, 1, 1);
        attach(value, 1, 1, 1, 1);


        doAdd = new Button("Add");
        doAdd.setHexpand(true);
        attach(doAdd, 0, 2, 2, 1);


        doAdd.addOnClicked(delegate void(Button _) {
            doAddPair(key.getText(), value.getText());
            key.setText("");
            value.setText("");
        });
    }
}