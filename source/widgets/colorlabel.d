/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.colorlabel;

import gtk.EventBox;
import gtk.Label;
import gdk.Color;
import data.restscopecolors;

class ColorLabel: EventBox {

    private Label label;

public:
    this(string text, Color color) {
        super();

        label = new Label(text);
        label.setMarginTop(10);
        label.setMarginBottom(10);
        label.setMarginLeft(14);
        label.setMarginRight(14);

        add(label);

        modifyBg(GtkStateType.NORMAL, color);
        label.modifyFg(GtkStateType.NORMAL, darkGray);
    }

    void setBgColor(Color color) {
        modifyBg(GtkStateType.NORMAL, color);
    }

    void setText(string text) {
        label.setText(text);
    }
}