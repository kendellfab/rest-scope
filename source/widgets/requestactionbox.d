/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.requestactionbox;

import gtk.Grid;
import models.storagerequest;
import gtk.Button;
import gtk.HSeparator;
import gtk.Label;
import gtk.Entry;

alias DoEditRequestName = void delegate(string requestName);
alias RequestDelete = void delegate(StorageRequest req);

class RequestActionBox: Grid {

    this(StorageRequest req, RequestDelete reqDel, DoEditRequestName doEditRequestName) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);
        setRowSpacing(10);
        setColumnSpacing(5);

        auto editLabel = new Label("New Request Name");
        auto editEntry = new Entry();
        editEntry.setPlaceholderText(req.name);

        auto doEditButton = new Button("Edit Request");
        doEditButton.addOnClicked(delegate void (Button _) {
            doEditRequestName(editEntry.getText());
            editEntry.setText("");
        });

        attach(editLabel, 0, 0, 1, 1);
        attach(editEntry, 1, 0, 1, 1);
        attach(doEditButton, 0, 1, 2, 1);
        
        auto hSep = new HSeparator();
        attach(hSep, 0, 2, 2, 1);

        auto delButton = new Button("Delete");
        attach(delButton, 0, 3, 2, 1);

        delButton.addOnClicked(delegate void(Button _) {
            reqDel(req);
        });

    }

}