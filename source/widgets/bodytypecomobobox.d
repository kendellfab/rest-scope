/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.bodytypecomobobox;

import gtk.ComboBox;
import data.bodytypeliststore;
import gtk.CellRendererText;
import gtk.TreeIter;
import data.bodytype;

class BodyTypeComboBox: ComboBox {

    this() {
        super(false);

        auto bodyTypeListStore = new BodyTypeListStore();

        auto cell = new CellRendererText();
        packStart(cell, true);
        addAttribute(cell, "text", 0);

        setModel(bodyTypeListStore);
        setActive(0);
    }

    string getBodyType() {
        TreeIter iter;

        getActiveIter(iter);

        auto data = getModel().getValueString(iter, 0);
        return data;
    }
}