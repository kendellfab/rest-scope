/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.projectscombobox;

import gtk.ComboBox;
import models.project;
import data.projectsliststore;
import gtk.CellRendererText;
import gtk.TreeIter;

class ProjectsComboBox: ComboBox {
private:
    ProjectsListStore projectsListStore;

public:
    this(ProjectsListStore projectsListStore) {
        super(false);

        this.projectsListStore = projectsListStore;

        auto cell = new CellRendererText();
        packStart(cell, true);
        addAttribute(cell, "text", 0);

        setModel(projectsListStore);
        setActive(0);
    }

    string getSelectedName() {
        TreeIter iter;

        getActiveIter(iter);
        auto data = getModel().getValueString(iter, 0);
        return data;
    }

    void updateProjectName(string projectName) {
        TreeIter iter;
        getActiveIter(iter);
        projectsListStore.updateProjectName(projectName, iter);
    }
}

