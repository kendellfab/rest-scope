/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.folderstreeeview;

import gtk.TreeView;
import data.foldersliststore;
import gtk.CellRendererText;
import gtk.TreeViewColumn;

class FoldersTreeView: TreeView {

private:
    FoldersListStore foldersListStore;

public:
    this(FoldersListStore foldersListStore) {
        super();
        this.foldersListStore = foldersListStore;
        setModel(foldersListStore);

        auto folderRenderer = new CellRendererText();
        auto folderColumn = new TreeViewColumn("Folder", folderRenderer, "text", 0);
        folderColumn.setResizable(true);
        appendColumn(folderColumn);

        auto requestRenderer = new CellRendererText();
        auto requestColumn = new TreeViewColumn("Request", requestRenderer, "text", 1);
        requestColumn.setResizable(true);
        appendColumn(requestColumn);
    }
}