/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.keyvaluetreeview;

import gtk.TreeView;
import gtk.TreeViewColumn;
import gtk.CellRendererText;
import gtk.CellRendererToggle;
import gtk.ListStore;
import gtk.TreePath;
import gtk.TreeIter;

alias ItemUpdatedCallback = void delegate();

class KeyValueTreeView: TreeView {

    ItemUpdatedCallback itemUpdatedCallback;

    this(ListStore store) {
        auto keyRenderer = new CellRendererText();
        keyRenderer.setProperty("editable", 1);
        keyRenderer.addOnEdited(delegate void(string p, string v, CellRendererText cell) {
            auto path = new TreePath(p);
            auto it = new TreeIter(store, path);
            store.setValue(it, 0, v);
            if(itemUpdatedCallback !is null) {
                itemUpdatedCallback();
            }
        });
        auto keyColumn = new TreeViewColumn("Name", keyRenderer, "text", 0);
        appendColumn(keyColumn);
        keyColumn.setResizable(true);

        auto valueRenderer = new CellRendererText();
        valueRenderer.setProperty("editable", 1);
        valueRenderer.setProperty("ellipsize", PangoEllipsizeMode.END);
        valueRenderer.addOnEdited(delegate void(string p, string v, CellRendererText cell) {
            auto path = new TreePath(p);
            auto it = new TreeIter(store, path);
            store.setValue(it, 1, v);
            if(itemUpdatedCallback !is null) {
                itemUpdatedCallback();
            }
        });
        auto valueColumn = new TreeViewColumn("Value", valueRenderer, "text", 1);
        valueColumn.setExpand(true);
        appendColumn(valueColumn);
        valueColumn.setResizable(true);

        auto activeRenderer = new CellRendererToggle();
        activeRenderer.addOnToggled(delegate void(string p, CellRendererToggle) {
            auto path = new TreePath(p);
            auto it = new TreeIter(store, path);
            auto val = it.getValueInt(2) ? 0 : 1;
            store.setValue(it, 2, val);
            if(itemUpdatedCallback !is null) {
                itemUpdatedCallback();
            }
        });
        // auto activeColumn = new TreeViewColumn("Active", activeRenderer, "active", 2);
        auto activeColumn = new TreeViewColumn();
        activeColumn.setTitle("Active");
        activeColumn.packStart(activeRenderer, 0);
        activeColumn.addAttribute(activeRenderer, "active", 2);
        appendColumn(activeColumn);
        activeColumn.setResizable(true);

        setModel(store);
        setShowExpanders(true);
    }

}