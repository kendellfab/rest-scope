/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.authtypecombobox;

import gtk.ComboBox;
import data.authtypeliststore;
import gtk.CellRendererText;
import gtk.TreeIter;
import data.authtype;

class AuthTypeComboBox: ComboBox {
    this() {
        super(false);

        auto authTypeListStore = new AuthTypeListStore();

        auto cell = new CellRendererText();
        packStart(cell, true);
        addAttribute(cell, "text", 0);
        setModel(authTypeListStore);
        setActive(0);
    }

    string getAuthType() {
        TreeIter iter;

        getActiveIter(iter);

        auto data = getModel().getValueString(iter, 0);
        return data;
    }
}