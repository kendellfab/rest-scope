/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.baseenvironmentsactionbox;

import gtk.Grid;
import models.environment;
import gtk.Label;
import gtk.Entry;
import gtk.Button;
import gtk.HSeparator;

alias DoDeleteEnvironment = void delegate(Environment environment);
alias DoEditEnvironmentName = void delegate(Environment environment, string nameEdit);
alias DoAddChild = void delegate(Environment environment, string childName);

class BaseEnvironmentsActionBox: Grid {
private:


public:
    this(Environment selectedEnvironment, DoDeleteEnvironment doDeleteEnvironment, DoEditEnvironmentName doEditEnvironmentName, DoAddChild doAddChild) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);

        setRowSpacing(15);
        setColumnSpacing(5);

        auto childLabel = new Label("Add Child Environment");
        auto childEntry = new Entry();
        childEntry.setPlaceholderText("Child name");

        auto doChildButton = new Button("Add Child");
        doChildButton.addOnClicked(delegate void(Button _) {
            doAddChild(selectedEnvironment, childEntry.getText());
        });

        attach(childLabel, 0, 0, 1, 1);
        attach(childEntry, 1, 0, 1, 1);
        attach(doChildButton, 0, 1, 2, 1);

        auto hSep = new HSeparator();

        attach(hSep, 0, 2, 2, 1);

        auto editLabel = new Label("New Environment Name");
        auto editEntry = new Entry();
        editEntry.setPlaceholderText(selectedEnvironment.name);

        auto doEditButton = new Button("Edit Environment Name");
        doEditButton.addOnClicked(delegate void(Button _) {
            doEditEnvironmentName(selectedEnvironment, editEntry.getText());
        });

        attach(editLabel, 0, 3, 1, 1);
        attach(editEntry, 1, 3, 1, 1);
        attach(doEditButton, 0, 4, 2, 1);

        auto editSep = new HSeparator();

        attach(editSep, 0, 5, 2, 1);

        auto doDeleteButton = new Button("Delete");
        doDeleteButton.addOnClicked(delegate void(Button _) {
            doDeleteEnvironment(selectedEnvironment);
        });

        attach(doDeleteButton, 0, 6, 2, 1);
    }
}