/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.environmentstreeview;

import gtk.TreeView;
import data.environmentstreestore;
import gtk.CellRendererText;
import gtk.TreeViewColumn;
import gtk.TreePath;
import gtk.TreeIter;

class EnvironmentsTreeView: TreeView {
private:
    EnvironmentsTreeStore environmentsTreeStore;
    TreeViewColumn baseColumn;
    TreeViewColumn childColumn;

public:
    this(EnvironmentsTreeStore environmentsTreeStore) {
        super();
        this.environmentsTreeStore = environmentsTreeStore;
        setModel(this.environmentsTreeStore);
        setVexpand(true);
        setActivateOnSingleClick(true);
        setShowExpanders(true);

        auto baseRenderer = new CellRendererText();
        baseColumn = new TreeViewColumn("Base", baseRenderer, "text", 0);
        baseColumn.setResizable(true);
        appendColumn(baseColumn);

        auto childRenderer = new CellRendererText();
        childColumn = new TreeViewColumn("Child", childRenderer, "text", 1);
        childColumn.setResizable(true);
        appendColumn(childColumn);
    }

    void activateFirst() {

        TreeIter iter;
        if(environmentsTreeStore.getIterFirst(iter)) {
            getSelection().selectIter(iter);
        }

        rowActivated(new TreePath("0"), baseColumn);
    }
}