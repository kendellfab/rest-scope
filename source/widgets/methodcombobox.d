/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.methodcombobox;

import gtk.ComboBox;
import data.methodliststore;
import gtk.CellRendererText;
import gtk.TreeIter;

class MethodComboBox: ComboBox {

    this() {
        super(false);
        auto methodListStore = new MethodListStore();

        auto cell = new CellRendererText();
        packStart(cell, true);
        addAttribute(cell, "text", 0);

        setModel(methodListStore);
        setActive(0);
    }

    string getMethod() {
        TreeIter iter;

        getActiveIter(iter);

        auto data = getModel().getValueString(iter, 0);
        return data;
    }

    void updateActive(string input) {
        for(int i = 0; i < MethodListStore.items.length; i++) {
            if(input == MethodListStore.items[i]) {
                setActive(i);
                break;
            }
        }
    }
}