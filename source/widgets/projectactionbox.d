/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.projectactionbox;

import gtk.Grid;
import gtk.Label;
import gtk.Button;
import gtk.Entry;

alias DoEditProjectName = void delegate(string projectName);

class ProjectActionBox: Grid {
private:
    Label editLabel;
    Entry editEntry;

public:
    this(string projectName, DoEditProjectName doEditProjectName) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);

        setRowSpacing(10);
        setColumnSpacing(5);

        editLabel = new Label("New Project Name");
        editEntry = new Entry();
        editEntry.setPlaceholderText(projectName);
        auto doEditButton = new Button("Edit Project");
        doEditButton.addOnClicked(delegate void(Button _) {
            doEditProjectName(editEntry.getText());
            editEntry.setText("");
        });

        attach(editLabel, 0, 0, 1, 1);
        attach(editEntry, 1, 0, 1, 1);
        attach(doEditButton, 0, 2, 2, 1);
    }
}
