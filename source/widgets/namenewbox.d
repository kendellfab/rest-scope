/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.namenewbox;

import gtk.Box;
import gtk.Grid;
import gtk.Label;
import gtk.Entry;
import gtk.Button;

alias DoAddName = void delegate(string name);

class NameNewBox: Grid {

private:
    Label nameLabel;
    Entry nameEntry;

public:
    this(DoAddName doAddName, string lablelText) {
        super();
        setMarginTop(10);
        setMarginRight(10);
        setMarginBottom(10);
        setMarginLeft(10);

        nameLabel = new Label(lablelText);
        nameEntry = new Entry();
        nameEntry.setHexpand(true);

        setRowSpacing(10);
        setColumnSpacing(5);

        attach(nameLabel, 0, 0, 1, 1);
        attach(nameEntry, 1, 0, 1, 1);

        auto doAdd = new Button("Add");
        attach(doAdd, 0, 1, 2, 1);

        doAdd.addOnClicked(delegate void(Button _) {
            doAddName(nameEntry.getText());
            nameEntry.setText("");
        });
    }

}