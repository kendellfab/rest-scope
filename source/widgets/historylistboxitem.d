/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.historylistboxitem;

import gtk.Box;
import data.guiresponse;
import widgets.colorlabel;
import data.restscopecolors;
import std.conv;
import gtk.Label;
import gtk.Button;
import std.format;
import gtk.ListBoxRow;
import datetimeformat;

alias DoDeleteResponse = void delegate(GuiResponse response, HistoryListboxItem item);

class HistoryListboxItem: ListBoxRow {
private:
    GuiResponse response;

public:
    this(GuiResponse rs, DoDeleteResponse deleteResponse) {
        super();

        auto box = new Box(GtkOrientation.HORIZONTAL, 2);
        setMarginLeft(2);
        setMarginTop(5);
        setMarginRight(2);
        setMarginBottom(5);
        this.response = rs;

        auto lblCode = new ColorLabel("--", yellow);

        box.packStart(lblCode, false, false, 0);

        if(rs.code >= 200 && rs.code < 300) {
            lblCode.setBgColor(green);
        } else if(rs.code >= 300 && rs.code < 400) {
            lblCode.setBgColor(blue);
        } else if(rs.code >= 400 && rs.code < 500) {
            lblCode.setBgColor(orange);
        } else {
            lblCode.setBgColor(red);
        }
        lblCode.setText(to!string(rs.code));

        auto duration = new ColorLabel(to!string(rs.connectTime + rs.recvTime) ~ " ms", gray);
        box.packStart(duration, false, false, 0);

        auto size = new ColorLabel(rs.formatBytes(), gray);
        box.packStart(size, false, false, 0);

        auto lblWhen = new Label(rs.at.format("Mmm dd, yyyy @ HH:ii"));
        lblWhen.setMarginStart(8);
        box.packStart(lblWhen, false, false, 0);

        auto btDelete = new Button("edit-delete-symbolic", GtkIconSize.MENU);
        btDelete.setMarginEnd(4);
        box.packEnd(btDelete, false, false, 0);

        btDelete.addOnClicked = (Button _) {
            deleteResponse(response, this);
        };

        add(box);
    }

    GuiResponse getResponse() {
        return response;
    }

}