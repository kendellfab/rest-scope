/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. If a copy of the MPL was not
 * distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
module widgets.environmentscombobox;

import gtk.ComboBox;
import data.environmentsliststore;
import models.environment;
import gtk.TreeIter;
import gtk.CellRendererText;
import std.stdio;

class EnvironmentsComboBox: ComboBox {

    EnvironmentsListStore envListStore;
    Environment[] envs;
    long selectedEnvironmentID;

    this(long selectedEnvironmentID) {
        super(false);
        this.selectedEnvironmentID = selectedEnvironmentID;

        envListStore = new EnvironmentsListStore();

        auto cell = new CellRendererText();
        packStart(cell, true);
        addAttribute(cell, "text", 0);

        setModel(envListStore);
        setActive(0);
    }

    void addEnvironments(Environment[] environments) {
        if(envs.length > 0) {
            selectedEnvironmentID = getSelectedEnv().id;
        }
        envListStore.clear();
        envs = [];
        foreach(Environment env; environments) {
            envs ~= env;
            envListStore.addEnvironment(env);
            foreach(Environment child; env.children) {
                envs ~= child;
                envListStore.addChildEnvironment(child);
            }
        }
        setActive(0);
        for(int i = 0; i < envs.length; i++) {
            if(envs[i].id == selectedEnvironmentID) {
                setActive(i);
            }
        }
    }

    Environment getSelectedEnv() {
        auto active = getActive();

        if(active < 0) {
            return null;
        }

        return envs[active];
    }

    public void reset() {
        envListStore.clear();
    }
}