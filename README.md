Rest Scope
==========

Rest scope is a linux first GTK+ native http/rest client.  If you have used Postman you are familiar with the use case for rest scope.  If you need help testing your rest endpoints and want an application that fits in with the Gnome desktop, then rest scope is the application for you.

![Dark Screenshot](resources/screenshots/screenshot-dark.png)
![Light Screenshot](resources/screenshots/screenshot-light.png)

## Building

Rest scope is built with the D programming language, using the gtk+ toolkit.  It also uses the GtkSourceView to display your request results.  Therefore you'll need gtk+ and the GtkSourceView installed on your machine to build.  You'll also need a D compiler and Dub (the d package manager) installed.

To build and run: dub build && ./restscope

## Flatpak

Flatpak is a planned release platform but that is still in the works, check back soon.

## Features

- Save your requests in a flat file.  This can be synced with you file cloud of choice (i.e. dropbox, google drive, own cloud, next cloud).
- Use environment variables in your url via mustache syntax ({{base_url}}/product/{{product_id}}).
- Multiple projects per database.